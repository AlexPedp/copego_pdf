{
    "opened": {
        "0": "Pending Payment",
        "1": "Order Paid"
    },
    "processing": {
        "2": "Preparation",
        "3": "Sent",
        "4": "Delivered"
    },
    "closed": {
        "5": "Closed",
        "6": "Archived\/Deleted",
        "7": "Aborted"
    },
    "aftersale": {
        "8": "AfterSale"
    }
}