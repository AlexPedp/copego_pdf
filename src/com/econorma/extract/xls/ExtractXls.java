package com.econorma.extract.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.UUID;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.econorma.data.OrdineCliente;
import com.econorma.testo.Testo;


public class ExtractXls {

	private String type;
	private String deposito = "";
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
	private OrdineCliente ordine;

	public void execute(File file) throws IOException{

		StringBuilder sb = new StringBuilder();
		
		FileInputStream inputStream = new FileInputStream(file);

		HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
		HSSFSheet  firstSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = firstSheet.iterator();
		
		type = getType(iterator);

		if (type == null){
			System.out.println("Ordine di cliente non valido");
			return ;
		}

		File csvFile = new File(type + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		
		iterator = firstSheet.iterator();
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			
			String art = null;
			boolean exit = false;
			
			switch (type) {
			case Testo.UNICOMM:
			
				
				if(nextRow.getRowNum() == 1){
					Cell cell1 = nextRow.getCell(3);
					String data = getCellValue(cell1);
					ordine = new OrdineCliente();
					ordine.setNumero("");
					ordine.setData(data);
					continue;
				}


				if(nextRow.getRowNum() == 4){
					Cell cell2 = nextRow.getCell(0);
					cell2.setCellType(Cell.CELL_TYPE_STRING);
					String valore = cell2.getStringCellValue();
					deposito = valore.split(" ")[2]; 
					continue;
				}

				if(nextRow.getRowNum() < 7){
					continue;  
				}

				Cell cell3 = nextRow.getCell(0);
				cell3 = nextRow.getCell(0);
				if (cell3 == null){
					continue;
				}
				
				art = getCellValue(cell3);
				if (art == null){
					continue;
				}
				
				
				break;
			case Testo.EUROSPIN:
			
				if(nextRow.getRowNum() < 19){
					continue;  
				}
				
				Cell cell1 = null;

				cell1 = nextRow.getCell(0);
				String string = cell1.getStringCellValue();
				if (string.contains(Testo.ATTENZIONE)){
					exit = true;
					break;
				}
				
				cell1 = nextRow.getCell(1);
				String colli = getCellValue(cell1);
				if (colli == null || colli == "0") {
					continue;
				}
				
				cell1 = nextRow.getCell(3);
				art = getCellValue(cell1);
				
				ordine = new OrdineCliente();
				ordine.setNumero("");
				ordine.setData("");
				
				break;
			}
			
			if (type == Testo.EUROSPIN && exit){
				break;
			}
			

			sb.append(type.trim());
			sb.append(';');
			sb.append(deposito.trim().toUpperCase());
			sb.append(';');
			sb.append(ordine.getNumero());
			sb.append(';');
			sb.append(ordine.getData());
			sb.append(';');
			sb.append(art.toUpperCase());
			sb.append(';'); 
			sb.append("");
			sb.append(';'); 


			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				String stringCellValue = getCellValue(cell);
				System.out.print(" - ");
				sb.append(stringCellValue);
				sb.append(';'); 	
			}

			sb.append('\n');
			System.out.println();
		}


		pw.write(sb.toString());
		pw.close();
		System.out.println("Done");

		workbook.close();
		inputStream.close();
	}


	public String getCellValue(Cell cell){

		String stringCellValue = null;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			stringCellValue = cell.getStringCellValue();
			System.out.print(cell.getStringCellValue());
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			System.out.print(cell.getBooleanCellValue());
			break;
		case Cell.CELL_TYPE_NUMERIC:
			stringCellValue = null;
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				stringCellValue = sdf.format(cell.getDateCellValue());
			} else {
				stringCellValue = String.valueOf(cell.getNumericCellValue());    	
			}

			System.out.print(cell.getNumericCellValue());
			break;
		case Cell.CELL_TYPE_FORMULA:
			stringCellValue = null;
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				stringCellValue = sdf.format(cell.getDateCellValue());
			} else {
				stringCellValue = String.valueOf(cell.getNumericCellValue());    	
			}

			System.out.print(cell.getNumericCellValue());
			break;
		}
		return stringCellValue;

	}
	
	private String getType(Iterator<Row> iterator)  throws IOException{

		String type = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();
			
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				
				
				String line = getCellValue(cell);
				if (line == null){
					continue;
				}
			
				if (line.toUpperCase().trim().contains(Testo.EUROSPIN)){
					return Testo.EUROSPIN;
				} else if (line.toUpperCase().trim().contains(Testo.UNICOMM)) {
					return Testo.UNICOMM; 
				}  	
			}
			
//
//			while(cellIterator.hasNext()){
//				Cell cell = cellIterator.next();
////				System.out.println("Row: "+cell.getRowIndex()+" ,Column: "+cell.getColumnIndex());
////				System.out.println(cell.getStringCellValue());
//
//				String line = cell.getStringCellValue();
//
//				if (line.toUpperCase().trim().contains(Testo.EUROSPIN)){
//					return Testo.EUROSPIN;
//				} else if (line.toUpperCase().trim().contains(Testo.UNICOMM)) {
//					return Testo.UNICOMM; 
////				} 
//
//			}

		}

		return type;

	}


}


