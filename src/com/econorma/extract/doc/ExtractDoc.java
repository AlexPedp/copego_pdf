package com.econorma.extract.doc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.UUID;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;

import com.econorma.testo.Testo;

public class ExtractDoc {

	public void execute(File file) throws IOException, ParseException{

		File csvFile = new File(Testo.VRM_FILE + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();

		FileInputStream fs= new FileInputStream(file.getAbsoluteFile());
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));


		HWPFDocument doc = new HWPFDocument(fs);
		WordExtractor extractor = new WordExtractor(doc);
		String delimiter = "\\s+";
		String text = extractor.getText();
		if (text != null) {
			boolean startReading = false;
			for (String line : text.split("\n|\r")) {

				if (line.toUpperCase().contains(Testo.SALUTI)){
					break;
				}

				if (line.isEmpty() || line.trim().length() == 0){
					continue;
				}

				if (startReading) {

					if (line.contains(Testo.VONGOLE_VERACI_3)){
						line = line.replaceAll(Testo.VONGOLE_VERACI_3, Testo.VONGOLE_VERACI_3_KG);
					}

					if (line.contains(Testo.RICCI) && line.contains(Testo.KG)){
						line = line.trim().replaceAll(Testo.KG, Testo.RICCI_KG);
					}

					int firstNumber=0;
					if (line.contains(Testo.KG)){
						int indexOf = line.indexOf(Testo.KG) + 2;
						String newLine = line.substring(indexOf, line.length());
						firstNumber = getFirstNumber(newLine) + indexOf;
					} else if(line.contains(Testo.GR)){
						int indexOf = line.indexOf(Testo.GR) + 2;
						String newLine = line.substring(indexOf, line.length());
						firstNumber = getFirstNumber(newLine) + indexOf;
					} else{	
						firstNumber = getFirstNumber(line);
					}

					if (firstNumber==0){
						continue;
					}

					System.out.println(line);
					String artDesc[] = line.substring(0, firstNumber-1).split(delimiter);
					String art = null;
					String desc = null;
					if (artDesc.length>=2){
						art = artDesc[0].toUpperCase();
						desc = artDesc[1].toUpperCase();	
					} else {
						art = artDesc[0].toUpperCase();
						desc = artDesc[0].toUpperCase();
					}


					if (line.contains(Testo.VONGOLE_VERACI_3_KG)){
						desc = desc + " X ";
					}

					if (line.contains(Testo.COZZE_ITALIA_SV)){
						desc = Testo.ITALIA_SV;
					}

					if (line.contains(Testo.COZZE_SPAGNA_SV)){
						desc = Testo.SPAGNA_SV;
					} else {
						if (artDesc.length>1){
							if (artDesc[1].contains(Testo.SPAGNA)){
								desc = Testo.SPAGNA;
							}
						}

					}

					line = line.toUpperCase().replaceAll("X", " ");
					String strArray[] = line.substring(firstNumber, line.length()).split(delimiter);
					if (strArray.length < 3){
						continue;
					}

					//					System.out.println(line);
					sb.append(Testo.VRM);
					sb.append(';');
					sb.append(Testo.VRM_DEPOSITO.trim());
					sb.append(';');
					sb.append("");
					sb.append(';');
					sb.append("");
					sb.append(';');
					sb.append(art + " " + desc);
					sb.append(';');
					sb.append(art + " " + desc);
					sb.append(';');
					sb.append(strArray[0]);
					sb.append(';');
					sb.append(strArray[1]);
					sb.append(';');
					sb.append(strArray[2]);
					sb.append('\n');
				}

				if (line.toUpperCase().contains(Testo.PRODOTTI)){
					startReading = true;
				}
			}


			if (br != null){
				br.close();
			}

			doc.close();
			extractor.close();

			pw.write(sb.toString());
			pw.close();
			System.out.println("Done");
		}

	}

	public int getFirstNumber(String data){

		for (int i = 0; i < data.length(); i++) {

			if (Character.isDigit(data.charAt(i))){
				return i;
			}

		}
		return 0;
	}

}


