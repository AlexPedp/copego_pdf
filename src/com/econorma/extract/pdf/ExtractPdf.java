package com.econorma.extract.pdf;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import com.econorma.data.OrdineCliente;
import com.econorma.logic.Parsers;
import com.econorma.testo.Testo;

public class ExtractPdf {

	private String type;
	private String deposito ="";
	private OrdineCliente ordine;
	private String genere;
	private ArrayList<String> ordini = new ArrayList<String>();
	private ArrayList<String> depositi = new ArrayList<String>();
	private Fiorital fiorital;

	public boolean execute(File file) throws IOException{

		type = getType(file);

		if (type == null){
			System.out.println("Ordine di cliente non valido");
			return false;
		}

		genere = getGenere(file);

		deposito = getDeposito(file);

		ordine = getOrdine(file);

		if (type.equals(Testo.LANDO)) {
			ordini = getOrdini(file);	
		}


		System.out.println("Ordine cliente valido: " + type);

		LinkedHashMap<Integer, Rectangle> rectangles = new LinkedHashMap<Integer, Rectangle>();
		Rectangle rect = new Rectangle();
		Rectangle rect2 = new Rectangle();
		Rectangle rect3 = new Rectangle();

		switch (type) {
		case Testo.AUCHAN:
			//			rect.setBounds(50, 180, 2000, 300);
			//			rect.setBounds(50, 120, 2000, 300);
			rect.setBounds(50, 180, 2000, 300);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.CARREFOUR:
			rect.setBounds(0, 240, 2000, 300);
			rectangles.put(new Integer(0), rect);
			rect2.setBounds(0, 240,2000, 300);
			rectangles.put(new Integer(1), rect2);
			break;
		case Testo.GS:
			rect.setBounds(0, 240, 2000, 300);
			rectangles.put(new Integer(0), rect);
			rect2.setBounds(0, 240,2000, 300);
			rectangles.put(new Integer(1), rect2);
			break;
		case Testo.GUSTOX:
			rect.setBounds(0, 500, 2000, 1000);
			rectangles.put(new Integer(0), rect);
			rect2.setBounds(0, 10, 2000, 1000);
			rectangles.put(new Integer(1), rect2);
			break;
		case Testo.LANDO:
			rect.setBounds(0, 0, 2000, 500);
			rectangles.put(new Integer(0), rect);
			rect2.setBounds(0, 0,2000, 500);
			rectangles.put(new Integer(1), rect2);
			rect2.setBounds(0, 0,2000, 500);
			rectangles.put(new Integer(2), rect2);
			rect2.setBounds(0, 0,2000, 500);
			rectangles.put(new Integer(3), rect2);
			rect2.setBounds(0, 0,2000, 500);
			rectangles.put(new Integer(4), rect2);
			break;
		case Testo.ESSELUNGA:
			switch (genere) {
			case Testo.NORMALE:
				rect.setBounds(50, 330, 2000, 500);
				break;
			case Testo.SOSTITUZIONE:
				rect.setBounds(50, 350, 2000, 400);
				break;
			default:
				rect.setBounds(30, 370, 2000, 500);
				break;
			}
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.FIORITAL:
			//			rect.setBounds(50, 330, 2000, 500);
			rect.setBounds(0, 330, 2000, 1000);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.AMBROS:
			rect.setBounds(30, 350, 2000, 200);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.CRISFISH:
			rect.setBounds(30, 280, 2000, 400);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.INTELLIGENTE:
			//			rect.setBounds(60, 260, 2000, 200);
			rect.setBounds(60, 255, 2000, 200);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.SMA_FILE:
			rect.setBounds(0, 250, 2000, 200);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.CENTRALE:
			rect.setBounds(0, 340, 2000, 500);
			rectangles.put(new Integer(0), rect);
			rect2.setBounds(0, 180, 2000, 1000);
			rectangles.put(new Integer(1), rect2);
			rect3.setBounds(0, 180, 2000, 1000);
			rectangles.put(new Integer(2), rect3);
			break;
		case Testo.COOP:
			rect.setBounds(0, 360, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.MEGAMARK:
			rect.setBounds(0, 340, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.CONAD:
			rect.setBounds(50, 320, 2000, 500);
			//			rect.setBounds(50, 340, 2000, 500);
			rectangles.put(new Integer(0), rect);
			rect2.setBounds(50, 80, 2000, 1000);
			rectangles.put(new Integer(1), rect2);
			rect3.setBounds(50, 80, 2000, 1000);
			rectangles.put(new Integer(2), rect3);
			break;
		case Testo.CONAD_VUOTO:
			rect.setBounds(0, 260, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.IPER:
			rect.setBounds(0, 150, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.DALMARE:
			//			rect.setBounds(0, 220, 2000, 200);
			//			rectangles.put(new Integer(0), rect);
			rect.setBounds(0, 330, 2000, 1000);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.TIGROS:
			rect.setBounds(0, 260, 2000, 200);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.WOERNDLE:
			rect.setBounds(0, 280, 2000, 200);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.ORTOFIN:
			rect.setBounds(0, 150, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.TOSANO:
			//			rect.setBounds(0, 280, 2000, 500);
			rect.setBounds(0, 230, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.EASY_FISH:
		case Testo.ARTELIER:
		case Testo.LORENA:
			rect.setBounds(0, 320, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.FVM_PESCA:
			rect.setBounds(0, 300, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.WORLD_FISH:
			rect.setBounds(0, 340, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.NEDERLOF:
			// OLD VERSION			
			rect.setBounds(0, 320, 2000, 500);
			//NUOVA VERSION
			//			rect.setBounds(0, 240, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.ONE_SERVICE:
			//			rect.setBounds(0, 300, 2000, 500);
			rect.setBounds(0, 250, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.SELECTA:
			rect.setBounds(0, 290, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.DESPAR:
			rect.setBounds(0, 60, 2000, 500);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.EUROSPIN:
			rect.setBounds(0, 20, 2000, 1000);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.ADC_SRL:
			rect.setBounds(0, 290, 2000, 1000);
			rectangles.put(new Integer(0), rect);
			break;
		case Testo.UNICOOP:
			switch (genere) {
			case Testo.NORMALE:
				rect.setBounds(0, 280, 2000, 500);
				rectangles.put(new Integer(0), rect);
				rect2.setBounds(0, 20, 2000, 1000);
				rectangles.put(new Integer(1), rect2);
				break;
			case Testo.SOSTITUZIONE:
				rect.setBounds(0, 300, 2000, 500);
				rectangles.put(new Integer(0), rect);
				rect2.setBounds(0, 20, 2000, 1000);
				rectangles.put(new Integer(1), rect2);
				break;
			default:
				rect.setBounds(0, 280, 2000, 500);
				break;
			}
			rectangles.put(new Integer(0), rect);
			break;
		default:
			break;
		}

		String text = "";
		for (Map.Entry<Integer, Rectangle> entry : rectangles.entrySet()) {
			Integer number = entry.getKey();
			Rectangle rectangle = entry.getValue();
			text += getText(file, rectangle, number);
			System.out.println(text);
		}



		String fname = file.getName();
		int pos = fname.lastIndexOf(".");
		if (pos > 0) {
			fname = fname.substring(0, pos);
		}

		File csvFile = new File(type.trim() + "_" +UUID.randomUUID() + ".csv");
		createCSV(csvFile, text);
		System.out.println("Done");
		return true;
	}

	private String getType(File pdfFile)  throws IOException{
		String type = null;
		PDDocument doc = PDDocument.load(pdfFile);
		String s =  new PDFTextStripper().getText(doc);
		
		boolean risc = false;
		boolean cons = false;
		String margherita = "";
		String dalMare = "";
		for (String line : s.split("\n|\r")) {
			if (line.toUpperCase().trim().contains(Testo.RISCONTRO_DDT)) {
				risc = true; 
			}
			if (line.toUpperCase().trim().contains(Testo.CONSEGNA_DIRETTA)) {
				cons = true; 
			}
			if (line.toUpperCase().trim().contains(Testo.DEP_ITTICO_CHIARI)) {
				margherita = Testo.SMA; 
			}
			if (line.toUpperCase().trim().contains(Testo.DEP_ITTICO_PORTO_VIRO) || line.toUpperCase().trim().contains(Testo.DEP_ITTICO_SAN_GIULIANO) || line.toUpperCase().trim().contains(Testo.DEP_ITTICO_GUIDONIA)) {
				margherita = Testo.AUCHAN; 
			}
			if (line.toUpperCase().trim().contains(Testo.SKIN)){
				dalMare = Testo.SKIN;
			}
		}

		for (String line : s.split("\n|\r")) {
			if (line.toUpperCase().trim().contains(Testo.AUCHAN_CODE) && margherita.equals(Testo.AUCHAN)){
				return Testo.AUCHAN;
			} else if (line.toUpperCase().trim().contains(Testo.COOP_CODICE) || (line.toUpperCase().trim().contains(Testo.COOP_RIVALTA))) {
				return Testo.COOP;
			} else if (line.toUpperCase().trim().contains(Testo.ESSELUNGA)) {
				return Testo.ESSELUNGA; 
			} else if (line.toUpperCase().trim().contains(Testo.GS_CARNE_MASSALENGO) || (line.toUpperCase().trim().contains(Testo.GS_CARNE_RIVALTA))) {
				return Testo.GS; 
			} else if (line.toUpperCase().trim().contains(Testo.CARREFOUR_MASSALENGO) || (line.toUpperCase().trim().contains(Testo.CARREFOUR_RIVALTA))   || 
					(line.toUpperCase().trim().contains(Testo.CARREFOUR_PESCE_AIROLA)) || (line.toUpperCase().trim().contains(Testo.CARREFOUR_PESCE_NORD_EST)) ||
					(line.toUpperCase().trim().contains(Testo.CARREFOUR_PESCE_ROMA)) || (line.toUpperCase().trim().contains(Testo.CARREFOUR_PESCE_TORINO)) ||
					(line.toUpperCase().trim().contains(Testo.CARREFOUR_PESCE_TRIBIANO)) || 
					(line.toUpperCase().trim().contains(Testo.CARREFOUR_PESCE_VOLLA))) {
				return Testo.CARREFOUR; 
			} else if (line.toUpperCase().trim().contains(Testo.FIORITAL) && !dalMare.equals(Testo.SKIN)) {
				return Testo.FIORITAL; 
			} else if (line.toUpperCase().trim().contains(Testo.AMBROS)) {
				return Testo.AMBROS;
			} else if (line.toUpperCase().trim().contains(Testo.ADC_SRL_CODE)) {
				return Testo.ADC_SRL;
			} else if (line.toUpperCase().trim().contains(Testo.CRISFISH)) {
				return Testo.CRISFISH; 
			} else if (line.toUpperCase().trim().contains(Testo.INTELLIGENTE)) {
				return Testo.INTELLIGENTE; 
			} else if (line.toUpperCase().trim().contains(Testo.SMA_NEW) && margherita.equals(Testo.SMA)) {
				return Testo.SMA_FILE; 
			} else if (line.toUpperCase().trim().contains(Testo.CENTRALE_PIVA) || line.toUpperCase().trim().contains(Testo.CENTRALE_PIVA_NEW)) {
				return Testo.CENTRALE; 
			} else if (line.toUpperCase().trim().contains(Testo.CONAD_ID) && (!line.toUpperCase().trim().contains(Testo.CONAD_IDCN))) {
				return Testo.CONAD;
			} else if (line.toUpperCase().trim().contains(Testo.CONAD_IDCN)) {
				return Testo.CONAD_VUOTO; 
			} else if (line.toUpperCase().trim().contains(Testo.IPER) && (!line.toUpperCase().trim().contains(Testo.IPER_CANALE)) || (line.toUpperCase().trim().contains(Testo.CONSEGNA_DIRETTA) && (line.toUpperCase().trim().contains(Testo.ORTOFIN)))) {
				return Testo.IPER; 
			} else if (line.toUpperCase().trim().contains(Testo.UNICOOP)) {
				return Testo.UNICOOP; 
			} else if (line.toUpperCase().trim().contains(Testo.FIORITAL) && dalMare.equals(Testo.SKIN)) {
				return Testo.DALMARE; 
			} else if (line.toUpperCase().trim().contains(Testo.TIGROS)) {
				return Testo.TIGROS; 
			} else if (line.toUpperCase().trim().contains(Testo.WOERNDLE)) {
				return Testo.WOERNDLE; 
			} else if (line.toUpperCase().trim().contains(Testo.ORTOFIN) && (!line.toUpperCase().trim().contains(Testo.CONSEGNA_DIRETTA)) && !risc && !cons) {
				return Testo.ORTOFIN; 
			} else if (line.toUpperCase().trim().contains(Testo.EASY_FISH_SRL)) {
				return Testo.EASY_FISH;
			} else if (line.toUpperCase().trim().contains(Testo.ARTELIER_SRL)) {
				return Testo.ARTELIER;
			} else if (line.toUpperCase().trim().contains(Testo.FVM_PESCA_SAS)) {
				return Testo.FVM_PESCA;
			} else if (line.toUpperCase().trim().contains(Testo.WORLD_FISH_SRL)) {
				return Testo.WORLD_FISH;
			} else if (line.toUpperCase().trim().contains(Testo.LORENA_22)) {
				return Testo.LORENA;
			} else if (line.toUpperCase().trim().contains(Testo.LANDO)) {
				return Testo.LANDO;
			} else if (line.toUpperCase().trim().contains(Testo.NEDERLOF)) {
				return Testo.NEDERLOF;
			} else if (line.toUpperCase().trim().contains(Testo.ONE_SERVICE_IVA) && line.trim().length()==4) {
				return Testo.ONE_SERVICE;
			} else if (line.toUpperCase().trim().contains(Testo.SELECTA)) {
				return Testo.SELECTA;
			} else if (line.toUpperCase().trim().contains(Testo.EUROSPIN)) {
				return Testo.EUROSPIN;
			} else if (line.toUpperCase().trim().contains(Testo.TOSANO)) {
				return Testo.TOSANO;
			} else if (line.toUpperCase().trim().contains(Testo.DESPAR_CODE)) {
				return Testo.DESPAR;
			} else if (line.toUpperCase().trim().contains(Testo.GUSTOX)) {
				return Testo.GUSTOX;
			} else if (line.toUpperCase().trim().contains(Testo.MEGAMARK_CODE)) {
				return Testo.MEGAMARK;
		}


		}

		return type;

	}

	private String getText(File file) throws IOException{
		PDDocument doc = PDDocument.load(file);
		PDPageTree pages = doc.getPages();
		int count = pages.getCount();
		PDPage page = pages.get(0); 
		Rectangle2D region = new Rectangle2D.Double(0, 330, 2000, 1000);
		String regionName = "region";
		PDFTextStripperByArea stripper;
		stripper = new PDFTextStripperByArea();
		stripper.setSortByPosition( true );
		stripper.addRegion(regionName, region);
		stripper.extractRegions(page);
		String s =  stripper.getTextForRegion("region");
		return s;

	}


	private String getGenere(File pdfFile)  throws IOException{
		String genere = Testo.NORMALE;
		PDDocument doc = PDDocument.load(pdfFile);
		//		doc.setAllSecurityToBeRemoved(true);
		String s =  new PDFTextStripper().getText(doc);

		if (type.equals(Testo.UNICOOP)){
			for (String line : s.split("\n|\r")) {
				if (line.toUpperCase().trim().contains(Testo.SOSTITUZIONE)){
					return Testo.SOSTITUZIONE;
				}
			}
		}
		if (type.equals(Testo.ESSELUNGA)){
			for (String line : s.split("\n|\r")) {
				if (line.toUpperCase().trim().contains(Testo.ANNULLA)){
					return Testo.SOSTITUZIONE;
				}
			}
		}

		return genere;

	}

	private ArrayList<String> getOrdini(File pdfFile)  throws IOException{

		PDDocument doc = PDDocument.load(pdfFile);
		String s =  new PDFTextStripper().getText(doc);

		for (String line : s.split("\n|\r")) {
			if (line.toUpperCase().trim().contains("FINE ORDINE N.")){
				String extractNumber = extractNumber(line);
				ordini.add(extractNumber);
			}
		}

		return ordini;
	}

	private String getDeposito(File pdfFile)  throws IOException{

		PDDocument doc = PDDocument.load(pdfFile);
		String s =  new PDFTextStripper().getText(doc);

		String search = null;

		switch (type) {
		case Testo.FIORITAL:
		case Testo.DALMARE:
			deposito = search1(s, search);
			break;
		case Testo.LANDO:
			search = "PUNTO DI CONSEGNA:";
			String[] searchA = {"NOALE","CONSELVE","VIGONZA", "VEGGIANO", "CAMPOSAMPIERO"};
			depositi = search4(s, search, searchA);
			break;
		case Testo.SELECTA:
			String[] searchB = {"OCCHIOBELLO"};
			deposito = search2(s, searchB);
			break;
		case Testo.EUROSPIN:
			String[] searchE = {"EUROSPIN"};
			deposito = search2(s, searchE);
			break;	
		case Testo.CARREFOUR:
		case Testo.GS:
			String[] search2 = {"PESCE-NORD-EST","MASSALENGO","PESCE-TORINO", "PESCE-ROMA", "PESCE-NORD-EST", "PESCE-AIROLA", "RIVALTA", "PESCE TRIBIANO", "PESCE-VOLLA"};
			deposito = search2(s, search2);
			break;
		case Testo.DESPAR:
			String[] searchO = {"AQUOLINA", "DESPAR"};
			deposito = search2(s, searchO);
			break;	
		case Testo.AUCHAN:
			search = "DEPOSITO:";
			deposito = search1(s, search);
			break;
		case Testo.ESSELUNGA:
			//			search = "O.P.";
			search = "CONSORZIO PESCATORI DI GORO";
			deposito = search1(s, search);
			break;
		case Testo.SMA_FILE:
			deposito = Testo.SMA_CODICE_DEPOSITO;
			break;
		case Testo.INTELLIGENTE:
			search = "FORNITORE";
			deposito = search3(s, search);
			break;
		case Testo.CENTRALE:
			String[] search3 = {"REGGIO","FORLI"};
			deposito = search2(s, search3);
			break;
		case Testo.COOP:
			deposito = Testo.COOP_DEPOSITO;
			break;
		case Testo.CRISFISH:
			deposito = Testo.CRISFISH;
			break;
		case Testo.ADC_SRL:
			String[] search11 = {"DELANCHY", "NS RITIRO"};
			deposito = search2(s, search11);
			if (deposito.isEmpty()){
				deposito = "NS RITIRO";
			}
			break;
		case Testo.CONAD:
			deposito = Testo.CONAD;
			break;
		case Testo.CONAD_VUOTO:
			//			deposito = Testo.CONAD_VUOTO;
			String[] search10 = {"CALCINATE", "CARNI"};
			deposito = search2(s, search10);
			break;
		case Testo.IPER:
			String[] search4 = {"UDINE","CASTELFRANCO"};
			deposito = search2(s, search4);
			break;
		case Testo.UNICOOP:
			String[] search5 = {"CAPALLE", "CAMPI BISENZIO"};
			deposito = search2(s, search5);
			break;
		case Testo.TIGROS:
			String[] search6 = {"CASSANO MAGNAGO"};
			deposito = search2(s, search6);
			break;
		case Testo.WOERNDLE:
			String[] search7 = {"CASTENEDOLO", "BOZEN"};
			deposito = search2(s, search7);
			break;
		case Testo.ORTOFIN:
			String[] search8 = {"LISCATE"};
			deposito = search2(s, search8);
			break;
		case Testo.TOSANO:
			String[] searchD = {"CHIOGGIA"};
			deposito = search2(s, searchD);
			if (deposito.isEmpty()){
				deposito = "CHIOGGIA";
			}
			break;
		case Testo.NEDERLOF:
			String[] searchC = {"MEDITERRANEA", "SEALOG", "FAVARO VENETO"};
			deposito = search2(s, searchC);
			break;
		case Testo.GUSTOX:
			String[] searchI = {"FISH SRL", "VERACE PESCHERIA", "CHINAPPI SRL", "MILLENIUM", "L'ATELIER", "FUTURA SRL", "LE COSE BUONE", "FAMILY FISH", "IL MORO 2000"};
			deposito = search2(s, searchI);
			break;
		case Testo.ONE_SERVICE:
			deposito = Testo.ONE_SERVICE;
			break;
		case Testo.EASY_FISH:
		case Testo.ARTELIER:
		case Testo.FVM_PESCA:
		case Testo.WORLD_FISH:
		case Testo.LORENA:
			String[] search9 = {"GUIDONIA", "ROMA", "FIUMICINO"};
			deposito = search2(s, search9);
			break;
		case Testo.MEGAMARK:
			String[] search12 = {"R/", "V/"};
			deposito = search2(s, search12);
			if (deposito.contains("R/")){
				deposito = "FASANO";
			}
			if (deposito.contains("V/")){
				deposito = "VOLLA";
			}
			break;
		default:
			break;
		}

		return deposito;

	}

	private OrdineCliente getOrdine(File pdfFile)  throws IOException{
		PDDocument doc = PDDocument.load(pdfFile);
		String s =  new PDFTextStripper().getText(doc);
		Parsers parsers = new Parsers(type, s, genere);
		ordine = parsers.run();
		return ordine;
	}

	private String search1(String s, String search){

		if (search != null) {
			for (String line : s.split("\n|\r")) {
				if (line.toUpperCase().trim().contains(search)){
					try {
						String extractNumber = extractNumber(line);
						return extractNumber;
					} catch (Exception e) {
						return "";
					}
				}
			}
		}

		return "";
	}

	private String search2(String s, String [] search){

		for (String row : search){
			for (String line : s.split("\n|\r")) {


				line = Normalizer.normalize(line, Normalizer.Form.NFD);
				line = line.replaceAll("[^\\p{ASCII}]", "");

				if (line.toUpperCase().contains(Testo.DEPOSITO)){
					String ok = null;
				}

				if (!type.equals(Testo.WOERNDLE) && line.toUpperCase().trim().contains(row) || type.equals(Testo.WOERNDLE) && line.toUpperCase().trim().contains(row) && !line.toUpperCase().contains(Testo.DEPOSITO)){
					try {
						return row;
					} catch (Exception e) {
						return "";
					}
				}
			}
		}

		return "";
	}

	private String search3(String s, String search){

		String extractNumber ="";
		if (search != null) {
			for (String line : s.split("\r")) {
				if (line.toUpperCase().trim().contains(search)){
					try {
						extractNumber = extractNumber(line);
					} catch (Exception e) {
						return "";
					}
				}
			}
		}

		return extractNumber;

	}

	private ArrayList<String> search4(String s, String search1, String [] search2){

		for (String row : search2){
			for (String line : s.split("\n|\r")) {

				if (line.toUpperCase().trim().contains(search1) && line.toUpperCase().trim().contains(row)){
					depositi.add(row);
				}
			}
		}

		return depositi;

	}


	private String getText(File pdfFile, Rectangle rect, int number) throws IOException {

		PDFTextStripperByArea stripper = new PDFTextStripperByArea();
		stripper.setSortByPosition( true );
		stripper.addRegion( "class1", rect );

		PDDocument doc = PDDocument.load(pdfFile);
		PDPageTree pages = doc.getPages();
		int count = pages.getCount()-1;
		String s ="";
		if (number <= count){
			PDPage page1 = pages.get(number);
			stripper.extractRegions(page1);
			s =  stripper.getTextForRegion( "class1" );
		}
		return s;

	}

	public String extractNumber(final String str) {                

		if(str == null || str.isEmpty()) return "";

		StringBuilder sb = new StringBuilder();
		boolean found = false;
		for(char c : str.toCharArray()){
			if(Character.isDigit(c)){
				sb.append(c);
				found = true;
			} else if(found){
				break;                
			}
		}

		return sb.toString();
	}


	public void createCSV(File csvFile, String content) throws FileNotFoundException{

		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();
		String delimiter = " ";
		int count = 0;

		String artSave = null;
		String descSave = null;
		String qtaSave = null;
		int indexSave=0;

		for (String line : content.split("\n|\r")) {
			if (!line.trim().isEmpty()) {

				if (!type.equals(Testo.LANDO) && !type.equals(Testo.EUROSPIN) && !type.equals(Testo.TOSANO) && !type.equals(Testo.DESPAR) && !type.equals(Testo.GUSTOX)) {
					if (line.toUpperCase().trim().contains(Testo.TOTALE) || line.trim().contains(Testo.VALORIZZAZIONE) || (line.trim().contains(Testo.SEPARATORE1) && !type.equals(Testo.INTELLIGENTE)) || line.trim().contains(Testo.SEPARATORE2) && (!type.equals(Testo.CONAD))){
						break;
					}
				}

				if (type.equals(Testo.UNICOOP)) {
					if (line.toUpperCase().trim().contains(Testo.DESCRIZIONE) || line.toUpperCase().trim().contains(Testo.BANCHINA) || line.toUpperCase().trim().contains(Testo.VALUTA)){
						continue;
					}
					if (line.toUpperCase().trim().contains(Testo.COLLO) || line.toUpperCase().trim().contains(Testo.PALLET) || line.toUpperCase().trim().contains(Testo.BANCARIO)){
						continue;
					}
					if (line.toUpperCase().trim().contains(Testo.UDM) || line.toUpperCase().trim().contains(Testo.PREZZO_ACQUISTO) || line.toUpperCase().trim().contains(Testo.COSTO_FATTURA)){
						continue;
					}
				}
				if (type.equals(Testo.COOP)) {
					if (line.toUpperCase().trim().contains(Testo.MODIFICATE) || line.toUpperCase().trim().contains(Testo.MODIFICATA)){
						continue;
					}
				}
				if (type.equals(Testo.WOERNDLE)) {
					if (line.toUpperCase().trim().contains(Testo.ASSOLVE)){
						break;
					}
				}
				if (type.equals(Testo.CENTRALE)) {
					if (line.toUpperCase().trim().length()<50){
						continue;
					}
					if (line.toUpperCase().contains(Testo.GALLOPROVINCIALIS)|| line.toUpperCase().contains(Testo.MYTILUS)){
						continue;
					}
				}
				if (type.equals(Testo.SMA_FILE)) {
					if (line.contains("COSTO NETTO")){
						continue;
					}
					String [] array =  {"ITALIA","FRANCIA","MEDITERRAN", "SPAGNA"};
					int index = StringUtils.indexOfAny(line, array);
					if (index != -1){
						continue;
					}
				}
				if (type.equals(Testo.CONAD)) {
					if (line.toUpperCase().contains("RIGA ARTICOLO") || line.toUpperCase().contains("SCALA SCONTI")){
						continue;
					}
				}

				if (type.equals(Testo.ONE_SERVICE)) {
					if (line.trim().length()<40){
						continue;
					}
					if (line.toUpperCase().contains(Testo.CODICE) && line.toUpperCase().contains(Testo.DESCRIZIONE)){
						continue;
					}
				}

				if (type.equals(Testo.LANDO)) {
					if (line.toUpperCase().trim().length()<30){
						continue;
					}
					if (line.toUpperCase().contains("RIEPILOGO")) {
						break;
					}
					if (line.toUpperCase().contains("ARTICOLO") || line.toUpperCase().contains("CONFERMA") || line.toUpperCase().contains("VIA") || line.toUpperCase().contains("TEL")){
						continue;
					}
					if (line.toUpperCase().contains("DATA ORDINE")) {
						continue;
					}
					if (line.toUpperCase().contains("DATA CONSEGNA") || line.toUpperCase().contains("FINE ORDINE")) {
						continue;
					}
					if (line.toUpperCase().contains("PUNTO DI CONSEGNA")){
						indexSave++;
						continue;
					}
				}

				if (type.equals(Testo.FIORITAL) || type.equals(Testo.DALMARE)) {

					fiorital = new Fiorital(line);

					if (fiorital.isFound()){
						artSave = fiorital.getItem();	
					}

					List<String> list =  Arrays.asList(line.trim().split("\\s+"));


					if (!list.contains("CP03") && !list.contains("CP05") && !list.contains("CP01") && !list.contains("PZ") && !list.contains("SC") && !list.contains("Scatol")){
						continue;
					}
				}

				if (type.equals(Testo.NEDERLOF)) {

					if (line.toUpperCase().contains("ART.FOR")){
						continue;
					}
				}

				if (type.equals(Testo.TOSANO)) {
					
					if (line.contains("0244488")) {
						String ok = null;
					}

					String checkLine = line.replace(" ", "");

					if (checkLine.toUpperCase().contains("COSTOFATTURA") || checkLine.toUpperCase().contains("LINEA:")){
						continue;
					}

					if (checkLine.toUpperCase().contains("X") && line.toUpperCase().contains("=")){
						continue;
					}

					if (checkLine.trim().length()<20){
						continue;
					}
					
					if (!checkLine.toUpperCase().contains("VONGOLA") && !checkLine.toUpperCase().contains("COZZ") && !checkLine.toUpperCase().contains("LUPINO")){
						continue;
					}
				}

				if (type.equals(Testo.ORTOFIN)) {

					if (line.toUpperCase().contains("CONSEGNARE LA MERCE")){
						break;
					}
				}

				if (type.equals(Testo.SELECTA)) {

					if (line.trim().length()<20){
						continue;
					}

					if (line.toUpperCase().contains("IMO")){
						continue;
					}
				}

				if (type.equals(Testo.EUROSPIN)) {

					if (line.trim().length()<20){
						continue;
					}

					if (line.contains(Testo.TOTALE)){
						break;
					}

					List<String> list =  Arrays.asList(line.trim().split("\\s+"));

					if (!list.contains("IF")){
						continue;
					}
				}

				if (type.equals(Testo.CRISFISH)) {

					if (line.trim().length()<20){
						continue;
					}

					if (!line.toUpperCase().contains("GO7")){
						continue;
					}
				}
				
				if (type.equals(Testo.ADC_SRL)) {

					if (line.trim().length()<20){
						continue;
					}
					if (line.toUpperCase().contains("ORD NR")){
						break;
					}
 
				}
				
				if (type.equals(Testo.DESPAR)) {

					if (line.trim().length()<20){
						continue;
					}
					if (line.toUpperCase().contains("----------------------") || 
							(line.toUpperCase().contains("DENOMINAZIONE")) ||
							(line.toUpperCase().contains("ALLEVATO")) ||
							(line.toUpperCase().contains("PESCATO")) ||
							(line.toUpperCase().contains("AQUOLINA"))){
						continue;
					}
					if (line.toUpperCase().contains("CONSEGNA")||(line.toUpperCase().contains("DESPAR"))){
						break;
					}
 
				}
				
				if (type.equals(Testo.GUSTOX)) {

					if (!line.trim().contains("CPG-") && !line.trim().contains("€")){
						continue;
					}
					if (line.toUpperCase().contains("CORRIERE")){
						break;
					}
 
				}


				try {

					String art = null;
					String desc = null;
					String qta = null;

					String art2 = null;
					String desc2 = null;
					String qta2 = null;

					String [] rowArray = null;
					boolean isNumeric = false;


					int indexOf=0;
					switch (type) {
					case Testo.LANDO:
						rowArray = line.toUpperCase().replaceAll("NAI", "").trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta =  rowArray[rowArray.length-2];
						deposito = depositi.get(indexSave-1);
						ordine.setNumero(ordini.get(indexSave-1));
						break;
					case Testo.SELECTA:
						indexOf = line.lastIndexOf("KG");	
						line = line.substring(0, indexOf-2);
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta =  rowArray[rowArray.length-1];
						break;
					case Testo.ADC_SRL:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta =  rowArray[rowArray.length-4];
						break;
					case Testo.MEGAMARK:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta =  rowArray[rowArray.length-3];
						break;
					case Testo.GUSTOX:
						rowArray = line.trim().split("\\s+");
						if (line.contains("CPG-")) {
							indexOf = line.indexOf("165");
							if (indexOf != -1) {
								String linez = line.substring(indexOf, line.length());
								rowArray= linez.trim().split("\\s+");
							}
								artSave = rowArray[0].replaceAll("[^0-9]", "");;
								descSave = rowArray[1] + " " + rowArray[2];
								if (!line.contains("€")) {
									continue;
								}
						} 
						if (line.contains("€")) {
							indexOf = line.indexOf("€");
							if (indexOf != -1) {
								line = line .substring(0, indexOf+1);
								rowArray= line.trim().split("\\s+");
								art = artSave;
								desc = descSave;
								qta =  rowArray[rowArray.length-3];
							} 
						}
						break;
					case Testo.DESPAR:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta =  rowArray[rowArray.length-3].replace("|", "");
						break;
					case Testo.EUROSPIN:
						String [] otherArray = null;
						rowArray = line.trim().split("\\s+");
						indexOf = line.indexOf("IF");
						if (indexOf != -1) {
							line = line .substring(indexOf+2, line.length());
							otherArray= line.trim().split("\\s+");
						} 
						art = rowArray[2];
						desc = rowArray[3] + " " + rowArray[4];
						qta =  otherArray[0];

						break;
					case Testo.FIORITAL:
					case Testo.DALMARE:

						if (line.contains("CP")){
							indexOf = line.indexOf("CP");	
						} else if (line.contains("PZ")){
							indexOf = line.indexOf("PZ");
						} else if (line.contains("SC")){
							indexOf = line.indexOf("SC");
						} else if (line.contains("Scatol")){
							line = line.replace("Scatol", "SC");
							indexOf = line.indexOf("SC");
						}

						line = line .substring(0, indexOf+2);

						rowArray = line.trim().split("\\s+");

						if (rowArray.length>=4){
							art = rowArray[0];

							indexOf = StringUtils.indexOfAny(art, fiorital.getItems());
							if (indexOf == -1) {
								art = artSave;
							}

							desc = rowArray[1];
							qta = rowArray[rowArray.length-2];	
						} else {
							art = artSave;
							desc = artSave;
							qta = rowArray[rowArray.length-2];
						}

						break;
					case Testo.AMBROS:
						art = line.substring(0, 10);
						indexOf = line.indexOf("KG");
						desc = line .substring(11, indexOf+2);
						qta = line.substring(indexOf+2, line.length());
						break;
					case Testo.CARREFOUR:
						String artArray[] = line.trim().split(delimiter);
						art = artArray[0];
						indexOf = line.indexOf("100");
						if (line.contains("100,")){
							indexOf=-1;
						}
						if (line.contains("VONGOLA VERACE FQC (ATM)") || line.contains("VONGOLE VERACI RETE FQC")) {
							indexOf=-1;
						}
						if (indexOf==-1){
							String[] newArray = Arrays.copyOfRange(artArray, artArray.length-8, artArray.length);
							desc = artArray[2] + " " + artArray[3];
							qta = Arrays.toString(newArray).replaceAll(", |\\[|\\]", " ");
						} else{
							desc = line .substring(8, indexOf+3);
							qta = line.substring(indexOf+2, line.length());
						}
						break;
					case Testo.GS:
						rowArray = line.trim().split(" ");
						art = rowArray[0];
						desc = rowArray[2] + rowArray[3] + rowArray[4];
						String[] newArray = Arrays.copyOfRange(rowArray, rowArray.length-5, rowArray.length);
						StringBuilder strb = new StringBuilder();
						for (int i = 0; i < newArray.length; i++) {
							strb.append(newArray[i]+ " ");
						}
						String newString = strb.toString();
						qta = rowArray[rowArray.length-6] + " " + newString ;
						break;
					case Testo.INTELLIGENTE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[2] + " " +rowArray[3];
						qta = rowArray[rowArray.length-4];

						break;
					case Testo.SMA_FILE:
						line = line.replaceAll("!", "");
						rowArray = line.trim().split("\\s+");
						StringBuilder item = new StringBuilder();
						for (int i = 0; i <= 3; i++) {
							boolean hasDigit = rowArray[i].matches(".*\\d+.*");
							if (hasDigit) {
								continue;
							}
							item.append(rowArray[i]);
							item.append(" ");
						}
						art = item.toString();
						desc = rowArray[0] + " " +rowArray[1] + " " + rowArray[2];
						qta = rowArray[rowArray.length-6] + " " + rowArray[rowArray.length-1];
						break;
					case Testo.CENTRALE:
						line = line.toUpperCase().replaceAll(Testo.NUOVA_POS, "");
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta = rowArray[rowArray.length-3];
						break;
					case Testo.COOP:
						count++;
						rowArray = line.trim().split("\\s+");
						if (count == 1){
							artSave = rowArray[1];
							descSave = rowArray[2];
							continue;
						} else {
							art = artSave;
							desc = descSave;
							isNumeric = StringUtils.isNumeric(rowArray[0]);
							if (isNumeric){
								qta = rowArray[0].replace(".", "");
							} else {
								qta = rowArray[1].replace(".", "");
							}
							count= 0;
						}
						break;
					case Testo.CONAD:
						art = line.substring(0, 10).trim();
						indexOf = line.indexOf("CT");
						desc = line .substring(11, indexOf+2);
						qta = line.substring(indexOf+2, line.length());
						break;
					case Testo.CONAD_VUOTO:
						int conta = StringUtils.countMatches(line, "CT");
						if (conta==1){
							indexOf = StringUtils.ordinalIndexOf(line, "CT", 2);
							if (indexOf==-1){
								indexOf = StringUtils.ordinalIndexOf(line, "CT", 1);
								if (indexOf==-1){
									indexOf = line.indexOf("PZ");
									if (indexOf==-1){
										indexOf = line.indexOf("CT");
									}
								}else {
									int indexOfPz = StringUtils.ordinalIndexOf(line, "PZ", 1);
									if (indexOfPz!=-1){
										if (indexOfPz<indexOf) {
											indexOf=indexOfPz;
										}

									}
								}
							}
						}
						if (conta==2){
							indexOf = line.indexOf("CT");							
						}

						String line2 = null;

						if (line.length() > 100){
							line2 = line.substring(indexOf+11, line.length());
						}

						line = line.substring(0, indexOf+10);
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc = rowArray[2];
						qta = rowArray[rowArray.length-2];

						if (line2 != null){
							rowArray = line2.trim().split("\\s+");
							art2 = rowArray[1];
							desc2 = rowArray[2];
							qta2 = rowArray[rowArray.length-2];
						}

						break;	
					case Testo.IPER:
						art = line.substring(0, 10).trim();
						indexOf = line.indexOf("ES");
						if (indexOf == -1){
							indexOf = line.indexOf("E1");
						}
						desc = line .substring(11, indexOf+4);
						qta = line.substring(indexOf+4, line.length());
						break;
					case Testo.CRISFISH:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc =  rowArray[1] + rowArray[2] ;
						qta = rowArray[rowArray.length-2];
						break;
					case Testo.UNICOOP:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc = rowArray[1] + " " + rowArray[2];
						qta =  rowArray[rowArray.length-3];
						break;
						//					case Testo.DALMARE:
						//						rowArray = line.trim().split("\\s+");
						//						art = rowArray[0];
						//						indexOf = line.indexOf("CP");
						//						desc = line .substring(art.length()+1, indexOf);
						//						qta = line.substring(indexOf, line.length());
						//						break;
					case Testo.TIGROS:
						line = line.replace(" ", "");
						Pattern p = Pattern.compile("\\p{Alpha}");
						Matcher m = p.matcher(line);
						if (m.find()) {
							int start =  m.start();
							art = line.substring(start-7, start);
						} else {
							art = line.substring(3, 10);
							isNumeric = StringUtils.isNumeric(line.substring(8, 9));
							if (isNumeric){
								art = line.substring(2, 9);	
							}
						}
						indexOf = line.indexOf("IF");
						desc = line.substring(8, indexOf);
						if (isNumeric){
							desc = line .substring(9, indexOf);	
						}
						qta = line.substring(indexOf+2, indexOf+6);
						break;
					case Testo.WOERNDLE:
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc =  rowArray[3] + rowArray[4] ;
						indexOf = line.indexOf("KG");
						line = line.substring(indexOf, line.length());
						rowArray = line.trim().split("\\s+");
						qta = rowArray[2];
						//						qta = rowArray[rowArray.length-3];
						break;
					case Testo.ORTOFIN:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc =  rowArray[1] + rowArray[2] ;
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.TOSANO:
						indexOf = line.indexOf("CB");
						if (indexOf == -1){
							indexOf = line.indexOf("CN");
						}
						line = line.substring(0, indexOf);
						rowArray = line.trim().split("\\s+");
						art = rowArray[1];
						desc =  rowArray[2] + rowArray[3] ;
						//						indexOf = line.indexOf("KG");
						//						if (indexOf!=-1){
						//							line = line.substring(indexOf+2, line.length());
						//						}
						//						rowArray = line.trim().split("\\s+");
						qta = rowArray[rowArray.length-4];
						break;
					case Testo.NEDERLOF:
						// OLD VERSION						
						//						indexOf = line.indexOf("KG");
						indexOf = line.lastIndexOf("KG");
						if (indexOf==-1){

						} else{
							line = line.substring(0, indexOf);
						}

						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc =  rowArray[1] + rowArray[2] ;
						qta = rowArray[rowArray.length-3];

						// NUOVA VERSIONE						
						//						rowArray = line.trim().split("\\s+");
						//						art = rowArray[0];
						//						desc =  rowArray[1] + rowArray[2] ;
						//						qta = rowArray[rowArray.length-4];
						break;

					case Testo.ONE_SERVICE:
						indexOf = line.lastIndexOf("pz");
						if (indexOf!=-1){
							line = line.substring(0, indexOf); 
						} else {
							indexOf = line.lastIndexOf("kg");	
							if (indexOf!=-1){
								line = line.substring(0, indexOf);
							}
						}
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc =  rowArray[1] + rowArray[2] ;
						qta = rowArray[rowArray.length-1];
						break;
					case Testo.EASY_FISH:
					case Testo.ARTELIER:
					case Testo.FVM_PESCA:
					case Testo.WORLD_FISH:
					case Testo.LORENA:
						rowArray = line.trim().split("\\s+");
						art = rowArray[0];
						desc =  rowArray[1] + " " + rowArray[2] ;
						qta = rowArray[rowArray.length-4];
						break;
					default:
						art = line.substring(0, 10);
						desc = line .substring(10, 30);
						qta = line.substring(40, line.length());
						boolean hasDigit = qta.matches(".*\\d+.*");
						if (!hasDigit) {
							continue;
						}
						break;
					}

					sb.append(type.trim());
					sb.append(';');
					sb.append(deposito.trim());
					sb.append(';');
					sb.append(ordine.getNumero().trim());
					sb.append(';');
					sb.append(ordine.getData().trim());
					sb.append(';');
					sb.append(art.trim());
					sb.append(';'); 
					sb.append(desc);
					sb.append(';'); 

					String strArray[] = qta.trim().split(delimiter);

					for (String s: strArray) {
						if (!s.isEmpty()){
							sb.append(s);
							sb.append(';'); 
						}
					}

					if (art2!=null && qta2!=null){
						sb.append('\n');
						sb.append(type.trim());
						sb.append(';');
						sb.append(deposito.trim());
						sb.append(';');
						sb.append(ordine.getNumero().trim());
						sb.append(';');
						sb.append(ordine.getData().trim());
						sb.append(';');
						sb.append(art2.trim());
						sb.append(';'); 
						sb.append(desc2);
						sb.append(';'); 
						sb.append(qta2);
						sb.append(';'); 
					}


				} catch (Exception e){

				}

				sb.append('\n');


			}

		}


		pw.write(sb.toString());
		pw.close();

	}

	public boolean isNumeric(String str)  {  
		try {  
			double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe) {  
			return false;  
		}  
		return true;  
	}

	public static class Fiorital {

		private String item;
		private boolean found;
		private String line;
		private final String[] items = new String[]{"COZF", "FASF", "VVEF", "VOBF", "CZSK", "VOSK", "TFMF", "SWCV", "OSTF"};

		public Fiorital(String line){
			this.line = line;
			search();
		}

		public void search(){

			int indexOf = StringUtils.indexOfAny(line, items);
			if (indexOf == -1) {
				found = false;
				return;
			}

			item = line.substring(indexOf, indexOf+7);
			found = true;


		}


		public String getItem() {
			return item;
		}


		public void setItem(String item) {
			this.item = item;
		}


		public boolean isFound() {
			return found;
		}


		public String[] getItems() {
			return items;
		}


	}



}
