package com.econorma.extract.txt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.econorma.data.OrdineCliente;
import com.econorma.logic.Parsers;
import com.econorma.testo.Testo;

public class ExtractTxt {

	private OrdineCliente ordine;
	private String type;
	private String deposito;
	private String csvName;

	public static void main(String[] args)  throws Exception {
		new ExtractTxt().execute(new File("result.txt"));
	}

	public void execute(File file) throws IOException{

		try {

			StringBuilder sb = new StringBuilder();
			String delimiter = " ";
			List<String> contents= new ArrayList<String>();

			FileInputStream fs= new FileInputStream(file.getAbsoluteFile());
			BufferedReader br = new BufferedReader(new InputStreamReader(fs));
			String line;

			StringBuilder  lines = new StringBuilder();
			while ((line = br.readLine()) != null) {
				lines.append(line);
				contents.add(line);
			}

			type = getType(contents);

			if (type == null){
				System.out.println("Ordine di cliente non valido");
				return;
			}

			ordine = getOrdine(lines);

			switch (type) {
			case Testo.SMA:
				deposito = Testo.SMA_DEPOSITO;
				csvName = Testo.SMA_FILE;
				break;
			case Testo.ESSELUNGA:
				deposito = Testo.ESSELUNGA;
				csvName = Testo.ESSELUNGA;
				break;
			case Testo.CONAD_QUILIANO:
				deposito = Testo.CONAD_QUILIANO;
				csvName = Testo.CONAD_QUILIANO;
				break;
			}

			File csvFile = new File(csvName + "_" +UUID.randomUUID() + ".csv");
			PrintWriter pw = new PrintWriter(csvFile);

			int i = 0;
			boolean startReading = false;

			for (String row: contents) {
				i++;

				if (type.equals(Testo.SMA)) {

					if (i< 18){
						continue;
					}

					if (row.replaceAll("\\s+","").length() < 50){
						continue;
					}

					if (row.toUpperCase().trim().contains(Testo.TOTALE) || row.toUpperCase().trim().contains(Testo.SEPARATORE)){
						break;
					}
					if (row.toUpperCase().trim().contains(Testo.ZONA)){
						continue;
					}

				}

				if (type.equals(Testo.ESSELUNGA)) {

					if (row.toUpperCase().trim().contains(Testo.TOTALE)){
						break;
					}

					if (row.toUpperCase().trim().contains(Testo.MITILUS) && row.trim().length()<30){
						continue;
					}

					if (row.toUpperCase().trim().contains(Testo.RUDITAPES) && row.trim().length()<30){
						continue;
					}

					if (row.toUpperCase().trim().contains(Testo.CHAMELEA) && row.trim().length()<30){
						continue;
					}

					if (row.toUpperCase().trim().contains(Testo.ARTICOLO)){
						startReading = true;
						continue;
					}

					if (!startReading){
						continue;
					}

					if (startReading && row.trim().length()<30){
						continue;
					}

				}

				if (type.equals(Testo.CONAD_QUILIANO)) {

					if (startReading && row.toUpperCase().trim().contains(Testo.TOTALE)){
						break;
					}

					if (row.toUpperCase().trim().contains(Testo.SCALA_SCONTI)){
						startReading = true;
						continue;
					}
					if (!startReading){
						continue;
					}

					if (startReading && row.trim().length()<30){
						continue;
					}
				}

				String [] rowArray = row.trim().split("\\s+");

				String art = null;
				String desc = null;
				String qta = null;

				switch (type) {
				case Testo.SMA:
					art = row.substring(2, 10);;
					desc = row.substring(10, 30);
					qta = row.substring(31, row.length()); 
					break;
				case Testo.CONAD_QUILIANO:
					String newLine = row.substring(row.indexOf("CT"), row.length());
					String [] newArray = newLine.trim().split("\\s+");

					art = rowArray[1];
					desc = rowArray[3];
					qta = newArray[1];
					break;
				case Testo.ESSELUNGA:
					try {
						String result = row.substring(row.lastIndexOf(",") + 1);

						if (result.substring(0, 1).equals(" ")){
							String firstPart = row.substring(0, row.lastIndexOf(',')+1);
							String secondPart = row.substring(row.lastIndexOf(',')+1,  row.length()).trim();
							row = firstPart +  secondPart;
							rowArray = row.trim().split("\\s+");
						}

						result = row.substring(row.indexOf(",") + 1);

						if (result.substring(0, 1).equals(" ")){
							String firstPart = row.substring(0, row.indexOf(',')+1);
							String secondPart = row.substring(row.indexOf(',')+1,  row.length()).trim();
							row = firstPart +  secondPart;
							rowArray = row.trim().split("\\s+");
						}

						result = row.substring(row.indexOf('.') - 1);

						if (result.substring(0, 1).equals(" ")){
							String firstPart = row.substring(0, row.indexOf('.')).trim();
							String secondPart = row.substring(row.indexOf('.'),  row.length()).trim();
							row = firstPart +  secondPart;
							rowArray = row.trim().split("\\s+");
						} 

					} catch (Exception e) {
						//					System.out.println(e);
					}

					boolean checkPoint = false;
					if (rowArray[rowArray.length-1].contains(".") || rowArray[rowArray.length-2].contains(".")) {
						checkPoint=true;
					}

					art = rowArray[0];
					desc = rowArray[1] + rowArray[2];
					if (!checkPoint) {
						qta = rowArray[rowArray.length-3];	
					} else {
						qta = rowArray[rowArray.length-4];
						if (!containsDigit(qta)){
							qta = rowArray[rowArray.length-3];	
						}
					}
					
					if (!containsDigit(qta)){
						continue;	
					}

					if (art.trim().equals("799881") && qta.trim().length()==0){
						Double value = Integer.parseInt(rowArray[rowArray.length-2].split("\\,")[0]) / 1.5;
						qta=String.valueOf(value).split("\\.")[0];
					}
					break;
				}

				if (art.trim().isEmpty()){
					continue;
				}

				sb.append(type);
				sb.append(';');
				sb.append(deposito);
				sb.append(';');
				sb.append(ordine.getNumero());
				sb.append(';');
				sb.append(ordine.getData());
				sb.append(';');
				sb.append(art.trim());
				sb.append(';'); 
				sb.append(desc.trim());
				sb.append(';');

				String strArray[] = qta.trim().split(delimiter);

				for (String s: strArray) {
					if (!s.isEmpty()){
						sb.append(s);
						sb.append(';'); 
					}
				}
				sb.append('\n');
			}

			if (br != null){
				br.close();
			}

			pw.write(sb.toString());
			pw.close();
			System.out.println("Done");

		} catch (Exception e) {
			System.out.println(e);
		}


	}

	private OrdineCliente getOrdine(StringBuilder sb)  throws IOException{
		Parsers parsers = new Parsers(type, sb.toString(), Testo.NORMALE);
		ordine = parsers.run();
		return ordine;
	}


	private String getType(List<String> contents)  {
		String type = null;

		for (String row: contents) {
			if (row.toUpperCase().trim().contains(Testo.SMA)){
				return Testo.SMA;
			} else if (row.toUpperCase().trim().contains(Testo.ESSELUNGA)) {
				return Testo.ESSELUNGA;
			} else if (row.toUpperCase().trim().contains(Testo.CONAD_QUILIANO)) {
				return Testo.CONAD_QUILIANO;
			}
		}

		return type;

	}  

	public final boolean containsDigit(String s) {
		boolean containsDigit = false;
		if (s != null && !s.isEmpty()) {
			for (char c : s.toCharArray()) {
				if (containsDigit = Character.isDigit(c)) {
					break;
				}
			}
		}
		return containsDigit;
	}



}


