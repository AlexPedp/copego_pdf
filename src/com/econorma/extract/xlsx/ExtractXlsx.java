package com.econorma.extract.xlsx;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.econorma.data.OrdineCliente;
import com.econorma.testo.Testo;


public class ExtractXlsx {

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private String type;
	private String deposito = "";
	private OrdineCliente ordine;
	private Workbook workbook;
	private Sheet sheet;
	private File csvFile;
	private PrintWriter pw;

	public void execute(File file) throws Exception{

		StringBuilder sb = new StringBuilder();

		FileInputStream inputStream = new FileInputStream(file);

		workbook = new XSSFWorkbook(inputStream);
		sheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = sheet.iterator();

		type = getType(iterator);

		if (type == null){
			System.out.println("Ordine di cliente non valido");
			return ;
		}

		iterator = sheet.iterator();
		deposito = getDeposito(iterator);

		iterator = sheet.iterator();
		ordine = getOrdine(iterator);

		if (!type.equals(Testo.LANDO)){
			csvFile = new File(type + "_" +UUID.randomUUID() + ".csv");
			pw = new PrintWriter(csvFile);	
		}

		iterator = sheet.iterator();
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			switch (type) {
			case Testo.METRO_ROMA:
			case Testo.METRO_MILANO:
			case Testo.METRO_VENEZIA:
				if(nextRow.getRowNum() < 5){
					continue;
				}
				break;
			case Testo.ORTOFIN:
				if(nextRow.getRowNum() < 4){
					continue;
				}
				break;
			case Testo.NUOVA_TIRRENIA:
				if(nextRow.getRowNum() < 9){
					continue;
				}
				break;
			case Testo.LANDO:
				if(nextRow.getRowNum() < 4){
					continue;
				}
				break;
			}

			if (type.contains(Testo.METRO)) {
				sb.append(Testo.METRO);
			} else {
				sb.append(type);	
			}
			sb.append(';');
			sb.append(deposito.trim());
			sb.append(';');
			sb.append(ordine.getNumero().trim());
			sb.append(';');
			sb.append(ordine.getData().trim());
			sb.append(';');

			int i = 0;
			boolean exit = false;
			boolean firstRow = true;

			if (type.equals(Testo.METRO_MILANO) || type.equals(Testo.METRO_ROMA)|| type.equals(Testo.METRO_VENEZIA)) {
				StringBuilder metro = readMetro(nextRow);
				sb.append(metro);
				continue;
			}

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				i++;
				String stringCellValue = null;

				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_STRING:
					stringCellValue = cell.getStringCellValue();
					System.out.print(cell.getStringCellValue());
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					System.out.print(cell.getBooleanCellValue());
					break;
				case Cell.CELL_TYPE_NUMERIC:
					stringCellValue = String.valueOf(cell.getNumericCellValue());
					System.out.print(cell.getNumericCellValue());

					if (type.equals(Testo.ORTOFIN) || type.equals(Testo.LANDO)){
						if (cell.getColumnIndex()==0){
							cell.setCellType(Cell.CELL_TYPE_STRING);
							stringCellValue = cell.getStringCellValue();
						}
					}
					break;
				case Cell.CELL_TYPE_BLANK:
					stringCellValue = String.valueOf(cell.getNumericCellValue());
					System.out.print(cell.getNumericCellValue());
					break;
				}
				System.out.print(" - ");

				if (type.equals(Testo.LANDO)){
					if (i==2 || i ==6 || i == 8 || i == 10 || i == 12 || i == 14){
						continue;
					}
				}

				if (type.equals(Testo.ORTOFIN)){
					if (i==1  && stringCellValue.equals("0.0")){
						int indexOf = sb.lastIndexOf("\n");
						if (indexOf != -1) {
							sb.delete(indexOf, indexOf + sb.length());
						}
						exit = true;    
						break;
					}	
				}
				if (type.equals(Testo.NUOVA_TIRRENIA)){
					String[] split = null;
					if (stringCellValue.contains("26") || stringCellValue.contains("21")) {
						split = stringCellValue.trim().split("\\s+");
						stringCellValue = split[split.length-1] + ";" + split[0] +  split[1];
						exit = true; 
					}
					if (!firstRow && stringCellValue.toUpperCase().contains("X")){
						split = stringCellValue.trim().toUpperCase().split("X");
						stringCellValue = split[0];
						if (stringCellValue.isEmpty()){
							stringCellValue = "0";
						}
					}
					firstRow = false;
				}


				sb.append(stringCellValue);
				sb.append(';'); 	

			}

			if (!exit){
				sb.append("\n");
				System.out.println();
			}

		}

		if (!type.equals(Testo.LANDO)){
			pw.write(sb.toString());
			pw.close();	
		} else{
			createOrdiniLando(sb);
		}

		System.out.println("Done");

		workbook.close();
		inputStream.close();
	}


	public OrdineCliente getOrdineMetro(Iterator<Cell> cellIterator){

		String cellName = null;
		int index = 0;

		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();

			String stringCellValue = null;
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				stringCellValue = cell.getStringCellValue();
				System.out.print(cell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				System.out.print(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				stringCellValue = String.valueOf(cell.getNumericCellValue());
				System.out.print(cell.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_BLANK:
				stringCellValue = String.valueOf(cell.getNumericCellValue());
				System.out.print(cell.getNumericCellValue());
				break;
			}
			System.out.print(" - ");


			if (type.equals(Testo.METRO_MILANO) || type.equals(Testo.METRO_ROMA)|| type.equals(Testo.METRO_VENEZIA)){
				if(stringCellValue.contains(Testo.AGREEMENT)){
					index = cell.getColumnIndex();
					cellName = (new CellReference(cell)).formatAsString();
				}
			}

//			if (type.equals(Testo.METRO_VENEZIA)){ 
//				if(stringCellValue.contains(Testo.ORDINE)){
//					index = cell.getColumnIndex();
//					cellName = (new CellReference(cell)).formatAsString();
//				}
//			}
		}

		for(int i=1; i < 10; i++) {

			String [] split = cellName.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
			String newCellName = split[0] + (Integer.parseInt(split[1]) + i);
			System.out.println(newCellName); 
			CellReference cr = new CellReference(newCellName);
			Row row = sheet.getRow(cr.getRow());
			Cell cell = row.getCell(cr.getCol());

			String stringCellValue="";
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				stringCellValue = cell.getStringCellValue();
				System.out.print(cell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				System.out.print(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				cell.setCellType(Cell.CELL_TYPE_STRING);
				stringCellValue = cell.getStringCellValue();
				System.out.print(stringCellValue);
				break;
			case Cell.CELL_TYPE_BLANK:
				stringCellValue = "";
				System.out.print(cell.getNumericCellValue());
				break;
			}

			if (type.equals(Testo.METRO_MILANO) || type.equals(Testo.METRO_ROMA)|| type.equals(Testo.METRO_VENEZIA)){
				stringCellValue=stringCellValue.replace("_", "").replace(".", "");
			}

			if (!stringCellValue.trim().isEmpty()){
				ordine = new OrdineCliente();
				ordine.setData(sdf.format(new Date()));
				ordine.setNumero(stringCellValue);
				return ordine;
			}

		}
		


		return null;

	}


	private String getType(Iterator<Row> iterator)  throws IOException{

		String type = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while(cellIterator.hasNext()){
				Cell cell = cellIterator.next();
				//				System.out.println("Row: "+cell.getRowIndex()+" ,Column: "+cell.getColumnIndex());
				//				System.out.println(cell.getStringCellValue());

				String line = cell.getStringCellValue();

				if (line.toUpperCase().trim().contains(Testo.METRO_MILANO)){
					return Testo.METRO_MILANO;
				} else if (line.toUpperCase().trim().contains(Testo.METRO_ROMA)){
					return Testo.METRO_ROMA;
				} else if (line.toUpperCase().trim().contains(Testo.METRO_VENEZIA)){
					return Testo.METRO_VENEZIA;
				} else if (line.toUpperCase().trim().contains(Testo.ORTOFIN)) {
					return Testo.ORTOFIN; 
				}  else if (line.toUpperCase().trim().contains(Testo.NUOVA_TIRRENIA_PESCA)) {
					return Testo.NUOVA_TIRRENIA; 
				}   else if (line.toUpperCase().trim().contains(Testo.LANDO)) {
					return Testo.LANDO; 
				}

			}

		}

		return type;

	}

	private String getDeposito(Iterator<Row> iterator)  throws IOException{

		String deposito = null;
		Cell cell = null;
		int i = 0;
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			i++;
			switch (type) {
			case Testo.METRO_ROMA:
			case Testo.METRO_MILANO:
			case Testo.METRO_VENEZIA:
				cell = nextRow.getCell(0);
				String[] split = cell.getStringCellValue().split(" ");
				deposito = split[1];
				break;
			case Testo.ORTOFIN:
				cell = nextRow.getCell(3);
				if (!cell.getStringCellValue().isEmpty() && i > 1){
					String[] split1 = cell.getStringCellValue().split(" ");
					deposito = split1[0];	
				}
				if (deposito == null && i > 1){
					deposito = Testo.ORTOFIN;
				}
				break;
			case Testo.NUOVA_TIRRENIA:
				deposito = Testo.NUOVA_TIRRENIA;
				break;
			case Testo.LANDO:
				deposito = Testo.LANDO;
				break;
			}

			if (deposito != null){
				return deposito;
			}


		}

		return deposito;


	}

	private OrdineCliente getOrdine(Iterator<Row> iterator)  throws IOException{

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			switch (type) {
			case Testo.METRO_ROMA:
			case Testo.METRO_MILANO:
			case Testo.METRO_VENEZIA:
				if(nextRow.getRowNum() == 4){
					ordine = getOrdineMetro(cellIterator );
				}
				break;
			case Testo.ORTOFIN:
			case Testo.NUOVA_TIRRENIA:
			case Testo.LANDO:
				ordine = new OrdineCliente();
				ordine.setData("");
				ordine.setNumero("");
				break;
			}


		}
		return ordine;
	}

	public void createOrdiniLando(StringBuilder sb) throws Exception{

		//		private HashMap<String, List<RowOrder>> documents = new HashMap<String, List<RowOrder>>();
		List<RowOrder> customer1 = new ArrayList<RowOrder>();
		List<RowOrder> customer2 = new ArrayList<RowOrder>();
		List<RowOrder> customer3 = new ArrayList<RowOrder>();
		List<RowOrder> customer4 = new ArrayList<RowOrder>();
		List<RowOrder> customer5 = new ArrayList<RowOrder>();


		String[] lines = sb.toString().split("\\n");
		for(String s: lines){
			String[] columns = s.split(";");

			if (columns[7] != null && Double.parseDouble(columns[7]) != 0.0) {
				RowOrder row = addRow(Testo.CAMPOSAMPIERO, columns, columns[7]);
				customer1.add(row);
			}
			if (columns[8] != null && Double.parseDouble(columns[8]) != 0.0){
				RowOrder row = addRow(Testo.VIGONZA, columns, columns[8]);
				customer2.add(row);
			}
			if (columns[9] != null && Double.parseDouble(columns[9]) != 0.0){
				RowOrder row = addRow(Testo.CONSELVE, columns, columns[9]);
				customer3.add(row);
			}
			if (columns[10] != null && Double.parseDouble(columns[10]) != 0.0){
				RowOrder row = addRow(Testo.NOALE, columns, columns[10]);
				customer4.add(row);
			}
			if (columns[11] != null && Double.parseDouble(columns[11]) != 0.0){
				RowOrder row = addRow(Testo.VEGGIANO, columns, columns[11]);
				customer5.add(row);
			}

		}

		createFileCsv(customer1);
		createFileCsv(customer2);
		createFileCsv(customer3);
		createFileCsv(customer4);
		createFileCsv(customer5);

	}

	public RowOrder addRow(String deposito, String[] columns, String qta){
		RowOrder row = new RowOrder();
		row.setCliente(Testo.LANDO);
		row.setNumeroOrdine("");
		row.setDataOrdine("");
		row.setArt(columns[4]);
		row.setDescrizione(columns[5]);
		row.setUm(columns[6]);
		row.setDeposito(deposito);
		row.setQta(qta);
		return row;
	}

	public class RowOrder {

		String cliente;
		String deposito;
		String numeroOrdine;
		String dataOrdine;
		String art;
		String descrizione;
		String um;
		String qta;

		public String getArt() {
			return art;
		}
		public void setArt(String art) {
			this.art = art;
		}
		public String getDescrizione() {
			return descrizione;
		}
		public void setDescrizione(String descrizione) {
			this.descrizione = descrizione;
		}
		public String getUm() {
			return um;
		}
		public void setUm(String um) {
			this.um = um;
		}
		public String getQta() {
			return qta;
		}
		public void setQta(String qta) {
			this.qta = qta;
		}
		public String getCliente() {
			return cliente;
		}
		public void setCliente(String cliente) {
			this.cliente = cliente;
		}
		public String getDeposito() {
			return deposito;
		}
		public void setDeposito(String deposito) {
			this.deposito = deposito;
		}
		public String getNumeroOrdine() {
			return numeroOrdine;
		}
		public void setNumeroOrdine(String numeroOrdine) {
			this.numeroOrdine = numeroOrdine;
		}
		public String getDataOrdine() {
			return dataOrdine;
		}
		public void setDataOrdine(String dataOrdine) {
			this.dataOrdine = dataOrdine;
		}

	}

	public void createFileCsv(List<RowOrder> customer) throws Exception {

		File csvFile = new File(type + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);
		StringBuilder sb = new StringBuilder();

		for (RowOrder r : customer){
			sb.append(r.getCliente());
			sb.append(';'); 	
			sb.append(r.getDeposito());
			sb.append(';'); 	
			sb.append(r.getNumeroOrdine());
			sb.append(';'); 	
			sb.append(r.getDataOrdine());
			sb.append(';'); 	
			sb.append(r.getArt());
			sb.append(';'); 	
			sb.append(r.getDescrizione());
			sb.append(';'); 	
			sb.append(r.getUm());
			sb.append(';');
			sb.append(r.getQta());
			sb.append(';'); 	
			sb.append("\n");
		}

		pw.write(sb.toString());
		pw.close();	
	}

	public StringBuilder readMetro(Row nextRow) {

		StringBuilder sb = new StringBuilder();

		int lastCellIndex = 35;  
		for (int x=0; x<=lastCellIndex; x++) {
			Cell cell = nextRow.getCell(x, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

			String stringCellValue = null;

			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				stringCellValue = cell.getStringCellValue();
				System.out.print(cell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				System.out.print(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				stringCellValue = String.valueOf(cell.getNumericCellValue());
				System.out.print(cell.getNumericCellValue());

				if (type.equals(Testo.ORTOFIN) || type.equals(Testo.LANDO)){
					if (cell.getColumnIndex()==0){
						cell.setCellType(Cell.CELL_TYPE_STRING);
						stringCellValue = cell.getStringCellValue();
					}
				}
				break;
			case Cell.CELL_TYPE_BLANK:
				stringCellValue = String.valueOf(cell.getNumericCellValue());
				System.out.print(cell.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_FORMULA:
				stringCellValue = null;
				String cellFormula = cell.getCellFormula();
				try {
					if (HSSFDateUtil.isCellDateFormatted(cell)) {
						stringCellValue = sdf.format(cell.getDateCellValue());
					} else {
						stringCellValue = String.valueOf(cell.getNumericCellValue());    
					}
				} catch (Exception e) {
					stringCellValue = ""; 
				}
				System.out.println(stringCellValue);
				break;
			}

			sb.append(stringCellValue);
			sb.append(';'); 	
		}

		sb.append("\n");
		return sb;
	}

}


