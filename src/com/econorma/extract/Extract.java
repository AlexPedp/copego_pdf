package com.econorma.extract;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import com.econorma.abby.Abby;
import com.econorma.extract.doc.ExtractDoc;
import com.econorma.extract.pdf.ExtractPdf;
import com.econorma.extract.txt.ExtractTxt;
import com.econorma.extract.xls.ExtractXls;
import com.econorma.extract.xlsx.ExtractXlsx;
import com.econorma.extract.xlsxm.ExtractXlsm;
import com.econorma.testo.Testo;



public class Extract {

	private final String TXT = "txt";
	private final String PDF = "pdf";
	private final String XLSX = "xlsx";
	private final String XLSM = "xlsm";
	private final String XLS = "xls";
	private final String DOC = "doc";
	private final String TIF = "tif";
	
	public void run(File file) throws IOException, ParseException, Exception{
		
		String extension  = getFileExtension(file);

		switch (extension.toLowerCase()) {
		case TXT:
			ExtractTxt txt = new ExtractTxt();
			txt.execute(file);
			break;
		case PDF:
			ExtractPdf pdf = new ExtractPdf();
			boolean found = pdf.execute(file);
			if (!found){
				Abby abby = new Abby();
				boolean result = abby.recognize(file);
				if (result){
					ExtractTxt extract = new ExtractTxt();
					File resultFile = new File(Testo.ABBY_RESULT); 
					extract.execute(resultFile);
					resultFile.delete();
				}
			}
			break;
		case TIF:
			Abby abby = new Abby();
			boolean result = abby.recognize(file);
			if (result){
				ExtractTxt extract = new ExtractTxt();
				File resultFile = new File(Testo.ABBY_RESULT); 
				extract.execute(resultFile);
				resultFile.delete();
			}
			break;
		case XLSX:
			ExtractXlsx xlsx = new ExtractXlsx();
			xlsx.execute(file);
			break;
		case XLSM:
			ExtractXlsm xlsm = new ExtractXlsm();
			xlsm.execute(file);
			break;
		case XLS:
			ExtractXls xls = new ExtractXls();
			xls.execute(file);
			break;
		case DOC:
			System.out.println(file.getAbsolutePath());
			ExtractDoc doc = new ExtractDoc();
			doc.execute(file);
			break;
		default:
			break;
		}

	}
	
	
	private String getFileExtension(File file) {
	    String name = file.getName();
	    try {
	        return name.substring(name.lastIndexOf(".") + 1);
	    } catch (Exception e) {
	        return "";
	    }
	}

}
