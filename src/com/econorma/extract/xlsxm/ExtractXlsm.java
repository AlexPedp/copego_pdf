package com.econorma.extract.xlsxm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.UUID;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.econorma.data.OrdineCliente;
import com.econorma.testo.Testo;


public class ExtractXlsm {

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private String type;
	private String deposito = "";
	private OrdineCliente ordine;
	private Workbook workbook;
	private Sheet sheet;
    private FormulaEvaluator objFormulaEvaluator;
    private DataFormatter objDefaultFormat;
	
	public void execute(File file) throws IOException{

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		
		
		StringBuilder sb = new StringBuilder();

		FileInputStream inputStream = new FileInputStream(file);

		workbook = new XSSFWorkbook(inputStream);
		sheet = workbook.getSheetAt(1);
		objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);
		objDefaultFormat = new DataFormatter();
		Iterator<Row> iterator = sheet.iterator();

//		newRead(workbook);
		
		type = getType(iterator);

		if (type == null){
			sheet = workbook.getSheetAt(0);
			iterator = sheet.iterator();
			type = getType(iterator);
			if (type == null){
				System.out.println("Ordine di cliente non valido");
				return ;
			}
		}

		iterator = sheet.iterator();
		deposito = getDeposito(iterator);
		
	    iterator = sheet.iterator();
//		ordine = getOrdine(iterator);

		File csvFile = new File(Testo.METRO + "_" +UUID.randomUUID() + ".csv");
		PrintWriter pw = new PrintWriter(csvFile);

		iterator = sheet.iterator();
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			boolean found = false;
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			switch (type) {
			case Testo.METRO_ROMA:
				if(nextRow.getRowNum() < 5){
					continue;
				}
				break;
		 
			}

			boolean firstTime = true;
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				 int column = cell.getColumnIndex();
				 String stringCellValue = null;
				DataFormatter formatter = new DataFormatter();  
	         	
	            switch (cell.getCellType()) {
	            case Cell.CELL_TYPE_STRING:
	            	stringCellValue = cell.toString();
	                break;
	            case Cell.CELL_TYPE_NUMERIC:
	                if (DateUtil.isCellDateFormatted(cell)) {
	                    SimpleDateFormat dateFormat = new SimpleDateFormat(
	                            "dd/MM/yyyy");
	                    stringCellValue = dateFormat.format(cell.getDateCellValue());
	                } else {
	                    Double value = cell.getNumericCellValue();
	                    Long longValue = value.longValue();
	                    stringCellValue = new String(longValue.toString());
	                }
	                break;
	            case Cell.CELL_TYPE_BOOLEAN:
	            	stringCellValue = new String(new Boolean(
	                        cell.getBooleanCellValue()).toString());
	                break;
	            case Cell.CELL_TYPE_FORMULA:
	            	stringCellValue = null;
	            	
	            	 switch(cell.getCachedFormulaResultType()) {
	                 case Cell.CELL_TYPE_NUMERIC:
	                	 stringCellValue = String.valueOf(cell.getNumericCellValue());
	                     break;
	                 case Cell.CELL_TYPE_STRING:
	                	 stringCellValue = String.valueOf(cell.getRichStringCellValue());
	                     break;
	             }
 
	                break;
	            case Cell.CELL_TYPE_BLANK:
	            	stringCellValue = "";
	                break;
	            }
	            
	            switch (type) {
				case Testo.METRO_ROMA:
					if (column == 1 && stringCellValue.contains("MITILO")){
						found = true;
					}
					break;	
				case Testo.METRO_MILANO:
					if (column == 1 && stringCellValue.contains("VONGOLA VERACE")){
						found = true;
					}
					break;
	            }
	            
	            if (found && firstTime){
	            	sb.append(Testo.METRO);
					sb.append(';');
					sb.append(deposito.trim());
					sb.append(';');
					sb.append("");
					sb.append(';');
					sb.append(sdf.format(calendar.getTime()));
					sb.append(';');	
					firstTime = false;
	            }

	            if (found) {
	               sb.append(stringCellValue);
	               sb.append(';');
	            }

			}
			
			if (found){
			 	sb.append('\n');
				System.out.println();	
			}
			
		}


		pw.write(sb.toString());
		pw.close();
		System.out.println("Done");

		workbook.close();
		inputStream.close();
	}


	public OrdineCliente getOrdineMetro(Iterator<Cell> cellIterator){

		String cellName = null;
		int index = 0;

		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();
	 

			String stringCellValue = null;
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				stringCellValue = cell.getStringCellValue();
				System.out.print(cell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				System.out.print(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_NUMERIC:
//				cell.setCellType(Cell.CELL_TYPE_STRING);
				stringCellValue = String.valueOf(cell.getNumericCellValue());
				System.out.print(cell.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_BLANK:
				stringCellValue = String.valueOf(cell.getNumericCellValue());
				System.out.print(cell.getNumericCellValue());
				break;
			}
			System.out.print(" - ");
 
		}
 
 
		return null;

	}


	private String getType(Iterator<Row> iterator)  throws IOException{

		String type = null;

		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			while(cellIterator.hasNext()){
				Cell cell = cellIterator.next();
 				System.out.println("Row: "+cell.getRowIndex()+" ,Column: "+cell.getColumnIndex());
 				System.out.println(cell.getStringCellValue());

				String line = cell.getStringCellValue();

				if (line.toUpperCase().trim().contains(Testo.METRO_ROMA)){
					return Testo.METRO_ROMA;
				}  
				if (line.toUpperCase().trim().contains(Testo.METRO_MILANO)){
					return Testo.METRO_MILANO;
				}  

			}

		}

		return type;

	}

	private String getDeposito(Iterator<Row> iterator)  throws IOException{

		String deposito = null;
		Cell cell = null;
		int i = 0;
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			i++;
			switch (type) {
			case Testo.METRO_ROMA:
			case Testo.METRO_MILANO:
 				cell = nextRow.getCell(0);
//				String[] split = cell.getStringCellValue().split(" ");
//				deposito = split[1];
				deposito = cell.getStringCellValue();
				break;
			 
			}
			
			if (deposito != null){
				return deposito;
			}
 

		}

		return deposito;


	}
	
	private OrdineCliente getOrdine(Iterator<Row> iterator)  throws IOException{
 		 
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			Iterator<Cell> cellIterator = nextRow.cellIterator();

			switch (type) {
			case Testo.METRO_ROMA:
				if(nextRow.getRowNum() == 59){
					ordine = getOrdineMetro(cellIterator );
				}
				break;
			case Testo.METRO_MILANO:
				if(nextRow.getRowNum() == 27){
					ordine = getOrdineMetro(cellIterator );
				}
				break;
			 
			}
		

		}
		return ordine;
	}
	
	 

}
 
