package com.econorma.storeden;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.econorma.data.Edetail;
import com.econorma.data.EdetailApp;
import com.econorma.data.Eitem;
import com.econorma.data.Eorder;
import com.econorma.testo.Testo;

public class StoredenCsv {
	
	private List<Eorder> eorders = new ArrayList<Eorder>();
//	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	private DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	private SimpleDateFormat sdfNew = new SimpleDateFormat("dd/MM/yyyy");
	private File csvFile;
	private PrintWriter pw;
	private StringBuilder sb;

	public StoredenCsv(List<Eorder> eorders) throws Exception {
		this.eorders = eorders;
	}

	public void openCsv() throws Exception{
		csvFile = new File(Testo.STOREDEN + "_" +UUID.randomUUID() + ".csv");
		pw = new PrintWriter(csvFile);
		sb = new StringBuilder();
	}

	public void closeCsv(){
		pw.write(sb.toString());
		pw.close();
		System.out.println("CVS file created: " +  csvFile);
	}

	public void execute() throws Exception{
		for (Eorder eorder: eorders){
			if (eorder.getStatus()!=0 && eorder.getStatus()!=1){
				continue;
			}
 			
			Edetail edetails = null;
			EdetailApp edetailsApp = null;
			try {
				edetails = eorder.getDetails();
				edetailsApp = eorder.getDetailsApp();
			} catch (Exception e) {
				System.out.println(e);
			}
			
			if (edetails==null && edetailsApp==null){
				continue;
			}
			
			openCsv();
			
			if (edetails!=null){
				Map<String, Eitem> items  = edetails.getCart().getItems();
				for (String key: items.keySet()) {
					Eitem eitem = items.get(key);
					addToCsv(eorder, key, eitem);
				}
			}
			
			if (edetailsApp!=null){
				 ArrayList<Eitem> itemsApp = edetailsApp.getCart().getItems();
				 
				 for (Eitem ei: itemsApp){
					 addToCsv(eorder, "", ei);
				 }
			}
			
			closeCsv();
		}
	}

	public void addToCsv(Eorder eorder, String itemId, Eitem eitem){
		
		try {
			Date parse = sdf.parse(eorder.getOrderDate());
			String orderDate = sdfNew.format(parse);
			String deliveryUserId = null;
			String vatDeliveryUser = null;
			String vatBillingUser = null;	
			String fullName = null;
			String addr = null;
			String zip = null;
			String city = null;
			String iso = null;
			String phone = null;
			String email = null;
			String vat = null;
			Double total=0.0;
			String couponCode=null;
			Double couponValue=0.0;
			String fullNameBill = null;
			String addrBill = null;
			String zipBill = null;
			String cityBill = null;
			String isoBill = null;
			String phoneBill = null;
			String emailBill = null;
			String vatBill = null;
			
			if (eorder.getDetails()!=null){
				deliveryUserId = eorder.getDetails().getDelivery().getUserUID();
				vatDeliveryUser = eorder.getDetails().getDelivery().getVat();
				vatBillingUser = eorder.getDetails().getBilling().getVat();
				fullName = eorder.getDetails().getDelivery().getFullname();
				addr = eorder.getDetails().getDelivery().getAddr1();
				zip = eorder.getDetails().getDelivery().getZip();
				city = eorder.getDetails().getDelivery().getCity();
				iso = eorder.getDetails().getDelivery().getStateISO();
				phone = eorder.getDetails().getDelivery().getPhone();
				email = eorder.getDetails().getDelivery().getEmail();
				vat = eorder.getDetails().getDelivery().getVat();
				couponCode = eorder.getDetails().getCart().getCouponCode();
				couponValue = eorder.getDetails().getCart().getSaleAmount();
				total = eorder.getDetails().getCart().getTotal();
				
				fullNameBill = eorder.getDetails().getBilling().getFullname();
				addrBill = eorder.getDetails().getBilling().getAddr1();
				zipBill = eorder.getDetails().getBilling().getZip();
				cityBill = eorder.getDetails().getBilling().getCity();
				isoBill = eorder.getDetails().getBilling().getStateISO();
				phoneBill = eorder.getDetails().getBilling().getPhone();
				emailBill = eorder.getDetails().getBilling().getEmail();
				vatBill = eorder.getDetails().getBilling().getVat();
			}
			
			if (eorder.getDetailsApp()!=null){
				deliveryUserId = eorder.getDetailsApp().getDelivery().getUserUID();
				vatDeliveryUser = eorder.getDetailsApp().getDelivery().getVat();
				vatBillingUser = eorder.getDetailsApp().getBilling().getVat();
				fullName = eorder.getDetailsApp().getDelivery().getFullname();
				addr = eorder.getDetailsApp().getDelivery().getAddr1();
				zip = eorder.getDetailsApp().getDelivery().getZip();
				city = eorder.getDetailsApp().getDelivery().getCity();
				iso = eorder.getDetailsApp().getDelivery().getStateISO();
				phone = eorder.getDetailsApp().getDelivery().getPhone();
				email = eorder.getDetailsApp().getDelivery().getEmail();
				vat = eorder.getDetailsApp().getDelivery().getVat();
				couponCode = eorder.getDetailsApp().getCart().getCouponCode();
				couponValue = eorder.getDetailsApp().getCart().getSaleAmount();
				total = eorder.getDetailsApp().getCart().getTotal();
				
				fullNameBill = eorder.getDetailsApp().getBilling().getFullname();
				addrBill = eorder.getDetailsApp().getBilling().getAddr1();
				zipBill = eorder.getDetailsApp().getBilling().getZip();
				cityBill = eorder.getDetailsApp().getBilling().getCity();
				isoBill = eorder.getDetailsApp().getBilling().getStateISO();
				phoneBill = eorder.getDetailsApp().getBilling().getPhone();
				emailBill = eorder.getDetailsApp().getBilling().getEmail();
				vatBill = eorder.getDetailsApp().getBilling().getVat();
			}
			
			sb.append(Testo.STOREDEN);
			sb.append(';');
			if (fullName!=null){
				sb.append(fullName.toUpperCase());	
			} else {
				sb.append(fullNameBill.toUpperCase());
			}
			
			sb.append(';');
			sb.append(eorder.getOrderID());
			sb.append(';');
			sb.append(orderDate);
			sb.append(';');
			sb.append(itemId);
			sb.append(';'); 
			sb.append(eitem.getTitle().toUpperCase());
			sb.append(';');
			sb.append(eitem.getCount());
			sb.append(';');
			sb.append(eitem.getPrice());
			sb.append(';');
			sb.append(deliveryUserId);
			sb.append(';');
			sb.append(fullName.toUpperCase());
			sb.append(';');
			sb.append(addr.toUpperCase());
			sb.append(';');
			sb.append(zip);
			sb.append(';');
			sb.append(city.toUpperCase());
			sb.append(';');
			sb.append(iso.toUpperCase());
			sb.append(';');
			sb.append(phone);
			sb.append(';');
			sb.append(email.toUpperCase());
			sb.append(';');
			sb.append(vat);
			sb.append(';');
			sb.append(total);
			sb.append(';');
			sb.append(couponCode);
			sb.append(';');
			sb.append(couponValue);
			
			if (vatBillingUser.trim().length()>0 && !vatBillingUser.trim().equals(vatDeliveryUser.trim())){ 
				sb.append(fullNameBill.toUpperCase());
				sb.append(';');
				sb.append(addrBill.toUpperCase());
				sb.append(';');
				sb.append(zipBill);
				sb.append(';');
				sb.append(cityBill.toUpperCase());
				sb.append(';');
				sb.append(isoBill.toUpperCase());
				sb.append(';');
				sb.append(phoneBill);
				sb.append(';');
				sb.append(emailBill.toUpperCase());
				sb.append(';');
				sb.append(vatBill);
				sb.append(';');
			}
			
			sb.append('\n');
			
		} catch (Exception e) {
			System.out.println(e);
		}
	
	}
 

}
