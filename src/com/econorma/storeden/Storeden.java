package com.econorma.storeden;

import java.lang.reflect.Type;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.time.DateUtils;

import com.econorma.data.Ecustomer;
import com.econorma.data.Edetail;
import com.econorma.data.EdetailApp;
import com.econorma.data.Eitem;
import com.econorma.data.Eorder;
import com.econorma.data.Estatus;
import com.econorma.data.Ordine;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class Storeden {

	public static final String BASE_URL = "https://connect.storeden.com/v1.1/";
	public static final int STATO_PREPARAZIONE = 2;
	public static final int STATO_CONSEGNATO = 4;
	private String key;
	private String exchange;
	private int subbedDays;
	
	public Storeden(String key, String exchange, int subbedDays){
		this.key=key;
		this.exchange=exchange;
		this.subbedDays=subbedDays;
	}

	public void run()  throws Exception{
	 	
		String orderJson = fetchData(BASE_URL + "orders/list.json" + "?since=" + Integer.toString(getFromDate()));
		List<Eorder> orders = getOrders(orderJson);

		for (Eorder eo: orders){
			if (eo.getStatus()!=0 && eo.getStatus()!=1){
				continue;
			}
 
			
			System.out.println("Order: " + eo.toString());
			String detailOrderJson = fetchData(BASE_URL + "orders/order.json?orderID=" + eo.getOrderID());
			
			Edetail edetails = null;
			EdetailApp edetailsApp = null;
			try {
				edetails = getDetails(detailOrderJson);	
			} catch (Exception e) {
				System.out.println(e);
				edetailsApp = getDetailsApp(detailOrderJson);	
			}
			
			if (edetails==null && edetailsApp==null){
				continue;
			}
			
			eo.setDetails(edetails);
			eo.setDetailsApp(edetailsApp);
			
		 			
			if (edetails!=null){
				System.out.println("Details: " + edetails.toString());
				Map<String, Eitem> items  = edetails.getCart().getItems();
				
				for (String key: items.keySet()) {
			    	Eitem eitem = items.get(key);
			    	   System.out.println("Items: " + eitem.getTitle() + "|" + eitem.getCount() + "|"  + eitem.getPrice() + "|" + eitem.getTax_profile_value());
			    }
				
			}
			
			if (edetailsApp!=null){
				System.out.println("Details: " + edetailsApp.toString());
				 ArrayList<Eitem> itemsApp = edetailsApp.getCart().getItems();
				 
				 for (Eitem ei: itemsApp){
					  System.out.println("Items: " + ei.getTitle() + "|" + ei.getCount() + "|"  + ei.getPrice() + "|" + ei.getTax_profile_value());
				 }
			}
		}
		
		StoredenCsv storeCsv = new StoredenCsv(orders);
		storeCsv.execute();
		
		updateStatus(orders, STATO_PREPARAZIONE);
	
//		String customerJson = fetchData(BASE_URL + "clients/list.json");
//		List<Ecustomer> ecustomers = getCustomers(customerJson);
//		for (Ecustomer ec: ecustomers){
//			System.out.println("Customer: " + ec.toString());
//		}
		
	}

	public List<Eorder> getOrders(String result) throws Exception{
		Type jsonType = new TypeToken<Collection<Eorder>>(){}.getType();
		List<Eorder> eorders = (List<Eorder>)new Gson().fromJson(result, jsonType);
		return eorders;

	}

	public Edetail getDetails(String result) throws Exception{
		Type jsonType = new TypeToken<Edetail>(){}.getType();
		Edetail edetail = (Edetail) new Gson().fromJson(result, jsonType);
		return edetail;
	}

	public EdetailApp getDetailsApp(String result) throws Exception{
		Type jsonType = new TypeToken<EdetailApp>(){}.getType();
		EdetailApp edetail = (EdetailApp) new Gson().fromJson(result, jsonType);
		return edetail;
	}
	
	
	public List<Ecustomer> getCustomers(String result){
		Type jsonType = new TypeToken<Collection<Ecustomer>>(){}.getType();
		List<Ecustomer> ecustomers = (List<Ecustomer>)new Gson().fromJson(result, jsonType);
		return ecustomers;
	}
 

	public String fetchData(String url){

		String result = null;
//		System.out.println(url);	
		try {
			
			try	{
			    // Create a trust manager that does not validate certificate chains
			    TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
			        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			            return null;
			        }
			        public void checkClientTrusted(X509Certificate[] certs, String authType) {
			        }
			        public void checkServerTrusted(X509Certificate[] certs, String authType) {
			        }
			    }
			    };

			    // Install the all-trusting trust manager
			    SSLContext sc = SSLContext.getInstance("SSL");
			    sc.init(null, trustAllCerts, new java.security.SecureRandom());
			    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			    // Create all-trusting host name verifier
			    HostnameVerifier allHostsValid = new HostnameVerifier() {
			        public boolean verify(String hostname, SSLSession session) {
			            return true;
			        }
			    };

			    // Install the all-trusting host verifier
			    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			} catch (NoSuchAlgorithmException e) {
			    e.printStackTrace();
			} catch (KeyManagementException e) {
			    e.printStackTrace();
			}
			 
			Client client = Client.create();

			WebResource webResource = client.resource(url);

			ClientResponse response = webResource.accept("application/json")
					.header("key", key)
					.header("exchange", exchange)
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
			}

			result = response.getEntity(String.class);

//			if (url.contains("orderID")){
//				System.out.println(result);	
//			}
 	
		} catch (Exception e) {
			System.out.println(e);
		}

		return result;

	}
	
	public int getFromDate(){
		Date dateFrom = DateUtils.addDays(new Date(),-subbedDays);
		Long epoch = dateFrom.getTime();
		return (int)(epoch/1000);
	}
	
	public void updateStatus(List<Eorder> orders, int status){
		for (Eorder eo: orders){
			if (eo.getStatus()!=0 && eo.getStatus()!=1){
				continue;
			}
			
			Estatus estatus = new Estatus();
			estatus.setOrderID(eo.getOrderID());
			estatus.setStatus(status);
			
			String orderId = eo.getOrderID().trim();
			
			Gson gson = new Gson();
			String jsonStr = gson.toJson(estatus);
			
			boolean result = postData(BASE_URL + "orders/order.json", orderId, status);
			 
		 
		}
	}
	
	public void updateOrdersStatus(List<Ordine> ordini) {

		List <Eorder> eorders = new ArrayList<Eorder>();
		for (Ordine o: ordini ) {
			Eorder eorder = new Eorder();
			eorder.setOrderID(o.getNumeroStoreden().trim());
			eorder.setStatus(1);
			eorders.add(eorder);
			System.out.println("Aggiornamento stato ordine: " + o.getNumeroStoreden());
		}	
		
		updateStatus(eorders, STATO_CONSEGNATO);	

	}
	
	public boolean postData(String url, String orderId, int status){
		
		boolean result = false;
		
		try {
//			System.out.println("Aggiorna ordine Storeden : " + orderId + "|" +  status);
			
			Client client = Client.create();

			WebResource webResource = client.resource(url);
			
			MultivaluedMapImpl values = new MultivaluedMapImpl();
			values.add("orderID", orderId);
			values.add("status", status);
			ClientResponse response = webResource
					.type(MediaType.APPLICATION_FORM_URLENCODED)
					.header("key", key)
					.header("exchange", exchange)
					.put(ClientResponse.class, values);

//			System.out.println(response.getStatus() + " " + response.getEntity(String.class));
			
			if (response.getStatus() != 200) {
				result = false;	
//				System.out.println("Failed : HTTP error code : " + response.getStatus());
			} 
			
			String output = response.getEntity(String.class);
			result = true;
			
			
		} catch (Exception e) {
			result = false;
			System.out.println(e);
		}
		
		return result;
		
		
	}

}
