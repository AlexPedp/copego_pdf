package com.econorma.main;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.econorma.dao.DAO;
import com.econorma.dao.DatabaseManager;
import com.econorma.data.Ordine;
import com.econorma.logic.Reader;
import com.econorma.properties.AppProperties;
import com.econorma.storeden.Storeden;

public class Main {


	public static void main(String[] args) throws IOException, ParseException, Exception {
		
		File f = new File("application.properties");
		if(!f.exists()) { 
			AppProperties.getInstance().create();
		}
		
		Reader reader = new Reader();
		reader.run();
		
		
		Map<String, String> map = AppProperties.getInstance().load();
		String key  = map.get("storeden_key");
		String exchange = map.get("storeden_exchange");
		int subbedDays = Integer.parseInt(map.get("storeden_subbed_days"));
		boolean isActive = Boolean.parseBoolean(map.get("storeden_active"));
		
		if (isActive) {
			Storeden storeden = new Storeden(key, exchange, subbedDays);
			storeden.run();
			
			DatabaseManager databaseManager = DatabaseManager.getInstance();
			DAO dao = DAO.create(databaseManager);
			databaseManager.init();
			dao.init(); 
			
			List<Ordine> ordini = dao.readOrdini();
			if (ordini !=null) {
				storeden.updateOrdersStatus(ordini);
			}
			
			for (Ordine ordine: ordini){
				 dao.updateOrdine(ordine);
			}
			
		}

	}
	

}
