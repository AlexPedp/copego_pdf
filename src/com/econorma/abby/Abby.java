package com.econorma.abby;
import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Vector;

import com.econorma.properties.AppProperties;
import com.econorma.testo.Testo;

public class Abby {

	private static Client restClient;
	
	public boolean recognize(File pdfFile){
		System.out.println("Process documents using ABBYY Cloud OCR SDK.\n");
	
		if (!checkAppId()) {
			return false;
		}
		
		try {
			
//			ClientSettings.setupProxy();
			
			restClient = new Client();
			restClient.serverUrl = "http://cloud.ocrsdk.com";
//			restClient.applicationId = Testo.ABBY_APPLICATION_ID;
//			restClient.password = Testo.ABBY_PASSWORD;
			
			Map<String, String> map = AppProperties.getInstance().load();
			restClient.applicationId  = (String) map.get("abby_id");
			restClient.password = (String) map.get("abby_password");
			
			
			String [] args = new String [] {"","",""};
			args[0] =Testo.ABBY_TYPE; 
			args[1] = pdfFile.getName(); 
			args[2] = Testo.ABBY_RESULT;

			Vector<String> argList = new Vector<String>(Arrays.asList(args));

			String mode = Testo.ABBY_TYPE;
			argList.remove(0);
			
			performRecognition(argList);
			
			
		}catch (Exception e) {
			System.out.println(e);
			return false;
		}

		
		return true;
		 
	}
	

	private static boolean checkAppId() {
		String appId = Testo.ABBY_APPLICATION_ID;
		String password = Testo.ABBY_PASSWORD;
		if (appId.isEmpty() || password.isEmpty()) {
			System.out
					.println("Error: No application id and password are specified.");
			System.out.println("Please specify them in ClientSettings.java.");
			return false;
		}
		return true;
	}
 
  
	private static void performRecognition(Vector<String> argList)
			throws Exception {
		String language = CmdLineOptions.extractRecognitionLanguage(argList);
		String outputPath = argList.lastElement();
		argList.remove(argList.size() - 1);
		// argList now contains list of source images to process

		ProcessingSettings.OutputFormat outputFormat = outputFormatByFileExt(outputPath);

		ProcessingSettings settings = new ProcessingSettings();
		settings.setLanguage(language);
		settings.setOutputFormat(outputFormat);

		Task task = null;
		if (argList.size() == 1) {
			System.out.println("Uploading file..");
			task = restClient.processImage(argList.elementAt(0), settings);

		} else if (argList.size() > 1) {

			// Upload images via submitImage and start recognition with
			// processDocument
			for (int i = 0; i < argList.size(); i++) {
				System.out.println(String.format("Uploading image %d/%d..",
						i + 1, argList.size()));
				String taskId = null;
				if (task != null) {
					taskId = task.Id;
				}

				Task result = restClient.submitImage(argList.elementAt(i),
						taskId);
				if (task == null) {
					task = result;
				}
			}
			task = restClient.processDocument(task.Id, settings);

		} else {
			System.out.println("No files to process.");
			return;
		}

		waitAndDownloadResult(task, outputPath);
	}

  
 

	private static void performTextFieldRecognition(Vector<String> argList)
			throws Exception {
		String language = CmdLineOptions.extractRecognitionLanguage(argList);
		String options = extractExtraOptions(argList);
		String outputPath = argList.lastElement();
		argList.remove(argList.size() - 1);
		// argList now contains list of source images to process

		TextFieldSettings settings = new TextFieldSettings();
		settings.setLanguage(language);
		if (options != null) {
			settings.setOptions(options);
		}

		// TODO - different processing options

		if (argList.size() != 1) {
			System.out.println("Invalid number of files to process.");
			return;
		}

		System.out.println("Uploading..");
		Task task = restClient.processTextField(argList.elementAt(0), settings);

		waitAndDownloadResult(task, outputPath);
	}
 

	/** 
	 * Wait until task processing finishes
	 */
	private static Task waitForCompletion(Task task) throws Exception {
		while (task.isTaskActive()) {

			Thread.sleep(5000);
			System.out.println("Waiting..");
			task = restClient.getTaskStatus(task.Id);
		}
		return task;
	}
	
	/**
	 * Wait until task processing finishes and download result.
	 */
	private static void waitAndDownloadResult(Task task, String outputPath)
			throws Exception {
		task = waitForCompletion(task);

		if (task.Status == Task.TaskStatus.Completed) {
			System.out.println("Downloading..");
			restClient.downloadResult(task, outputPath);
			System.out.println("Ready");
		} else if (task.Status == Task.TaskStatus.NotEnoughCredits) {
			System.out.println("Not enough credits to process document. "
					+ "Please add more pages to your application's account.");
		} else {
			System.out.println("Task failed");
		}

	}

		/**
	 * Extract extra RESTful options from command-line parameters. Parameter is
	 * removed after extraction
	 * 
	 * @return extra options string or null
	 */
	private static String extractExtraOptions(Vector<String> args) {
		// Extra options parameter has from --options=<options>
		return CmdLineOptions.extractParameterValue("options", args);
	}

	/**
	 * Extract output format from extension of output file.
	 */
	private static ProcessingSettings.OutputFormat outputFormatByFileExt(
			String filePath) {
		int extIndex = filePath.lastIndexOf('.');
		if (extIndex < 0) {
			System.out
					.println("No file extension specified. Plain text will be used as output format.");
			return ProcessingSettings.OutputFormat.txt;
		}
		String ext = filePath.substring(extIndex).toLowerCase();
		if (ext.equals(".txt")) {
			return ProcessingSettings.OutputFormat.txt;
		} else if (ext.equals(".xml")) {
			return ProcessingSettings.OutputFormat.xml;
		} else if (ext.equals(".pdf")) {
			return ProcessingSettings.OutputFormat.pdfSearchable;
		} else if (ext.equals(".docx")) {
			return ProcessingSettings.OutputFormat.docx;
		} else if (ext.equals(".rtf")) {
			return ProcessingSettings.OutputFormat.rtf;
		} else {
			System.out
					.println("Unknown output extension. Plain text will be used.");
			return ProcessingSettings.OutputFormat.txt;
		}
	}

 

}
