package com.econorma.logic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;

import com.econorma.extract.Extract;

public class Reader {

	public void run() throws IOException, ParseException, Exception {

		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		
		File dir = new File(s);
		File finder[] = dir.listFiles(new FileExtensionFilter(".pdf", ".txt", ".xls", ".xlsx", ".xlsm", ".doc", ".tif"));
		
		for (File file: finder){
			if (!file.isHidden()){
				Extract extract = new Extract();
				extract.run(file);	
			}
			
		}
	}


//	private File[] finder(String dirName){
//
//		File dir = new File(dirName);
//
//		File[] files = dir.listFiles(new FileFilter() {
//			private final FileNameExtensionFilter filter =
//					new FileNameExtensionFilter("pdf","txt", "xlsx", "xls");
//			public boolean accept(File file) {
//				return filter.accept(file);
//			}
//		});
//
//		return files;
//
//	}
	

}
