package com.econorma.logic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.econorma.data.OrdineCliente;
import com.econorma.testo.Testo;

public class Parsers {

	private String type;
	private String text;
	private OrdineCliente ordine;
	private String regex;
	private String genere;
	private SimpleDateFormat fromFormat = new SimpleDateFormat("dd/MM/yy");
	private SimpleDateFormat toFormat = new SimpleDateFormat("dd/MM/yyyy");


	public Parsers(String type, String text, String genere){
		this.text = text;
		this.type = type;
		this.genere = genere;
	}


	public OrdineCliente run(){
		ordine = new OrdineCliente();
		ordine.setData("");
		ordine.setNumero("");

		String search = "";
		String search1 = "";
		String search2 = " ";

		switch (type) {
		case Testo.FIORITAL:
		case Testo.DALMARE:
			search = "NR.ORDINE";
			regex = "([a-zA-Z0-9\\./]+)(.*)";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.GUSTOX:
			ordine.setNumero("0");
			ordine.setData(toFormat.format(new Date()));
			break;
		case Testo.MEGAMARK:
			search = "CONFERMA ORDINE ACQUISTO";
			regex = "(.*)(\\d{2}/\\d{2}/\\d{2})";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.CONAD_QUILIANO:
			search = "ORDINE NR.";
			regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.DESPAR:
			search = "OF";
			regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.EUROSPIN:
			search = "N.AVV.CARICO";
			regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
			ordine = searchOrdine(text, search, regex);
			if (ordine==null){
				regex = "([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})(.*)";
				ordine = searchOrdine(text, search, regex);
			}
			break;
		case Testo.CRISFISH:
			ordine = searchOrdineCrisFish(text);
			break;
		case Testo.TOSANO:
			search = "ORDINE A FORNITORE";
			regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.SELECTA:
			regex = "(.*)(\\d{2}/\\d{2}/\\d{4})(.*)(\\d{2}/\\d{2}/\\d{4})";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.ADC_SRL:
			String searchX = "ORDINE";
			String searchY = "NUMERO";
		 	ordine = searchOrdineAdc(text, searchX, searchY);
			break;
		case Testo.CARREFOUR:
		case Testo.GS:
			search = "DATA ORDINE";
			ordine = searchOrdineAfterLine(text, search);
			break;
		case Testo.AUCHAN:
			search = "PROTOCOLLO";
			regex = "([0-9]+)(.*)";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.ESSELUNGA:
			search = "ACQUISTO N.RO";
			regex =  "[ ]+N.RO ([a-zA-Z0-9\\./]+)(.*)";
			ordine = searchOrdine(text, search, regex);
			if (ordine == null){
				search = "ACQUISTO";
				regex =  "[ ]+ ([a-zA-Z0-9\\./]+)(.*)";
				ordine = searchOrdine(text, search, regex);
			}
			break;
		case Testo.SMA_FILE:
			search = "ORDINE NUMERO";
			regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
			ordine = searchOrdine(text, search, regex);
			if (ordine == null){
				regex = "([0-9]+)(.*)(\\d{1,2}/\\d{1,2}/\\d{4})(.*)";
				ordine = searchOrdine(text, search, regex);
			}
			break;
		case Testo.ONE_SERVICE:
			search = "ORDINE";
			regex = "([0-9]+)(.*)";
			ordine = searchOrdine(text, search, regex);
			if (ordine == null){
				regex = "([0-9]+)(.*)(\\d{1,2}/\\d{1,2}/\\d{4})(.*)";
				ordine = searchOrdine(text, search, regex);
			}
			break;
		case Testo.NEDERLOF:
			search1 = "VIA ANTONIO BRUGNOLI 298";
			search2 = "00040400384";
			ordine = searchOrdineNederlof(text, search1, search2);
			break;
		case Testo.INTELLIGENTE:
			search = "CARICO";
			regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
			ordine = searchOrdine(text, search, regex);
			if (ordine == null){
				regex = "(.?)([0-9]+)(.*)(\\d{1,2}/\\d{2}/\\d{4})(.*)";
				ordine = searchOrdine(text, search, regex);
			}
			break;
		case Testo.CENTRALE:
			search = "N. ORD. ACQ.";
			ordine = searchOrdineAfterLine(text, search);
			break;
		case Testo.COOP:
			search = "N°ORDINE:";
			regex = "([0-9]+)(.*)(\\d{2}.\\d{2}.\\d{4})(.*)";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.CONAD:
			//			search = "CONTRATTO";
			//			ordine = searchOrdineBeforeLine(text, search);
			regex = "([0-9]+)(.)(\\d{2}/\\d{2}/\\d{4})(.)(\\d{2}/\\d{2}/\\d{4})";
			ordine = searchOrdine(text, "", regex);
			if (ordine==null) {
				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.)(\\d{2}/\\d{2}/\\d{4})";
				ordine = searchOrdine(text, "", regex);
			}
			break;
		case Testo.CONAD_VUOTO:
			search = "ORDINE NR.";
			ordine = searchOrdineInLine(text, search);
			break;
		case Testo.IPER:
			search = "PAG 1";
			ordine = searchOrdineBeforeLine(text, search);
			break;
//		case Testo.DALMARE:
//			search = "AQ";
//			ordine = searchOrdineCustom(text, search);	
//			break;
		case Testo.TIGROS:
			search = "ORDINE";
			ordine = searchOrdineInLine(text, search);
			break;
		case Testo.WOERNDLE:
			regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.EASY_FISH:
		case Testo.ARTELIER:
		case Testo.FVM_PESCA:
		case Testo.LORENA:
			regex = "(.?)([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{4})(.*)";
			ordine = searchOrdine(text, search, regex);
			break;
		case Testo.UNICOOP:

			switch (genere) {
			case Testo.NORMALE:
				search = Testo.ORDINE;
				ordine = searchOrdineAfterLine(text, search);	
				break;
			case Testo.SOSTITUZIONE:
				search = Testo.SOSTITUZIONE;
				ordine = searchOrdineAfterLine(text, search);
				break;
			default:
				search = Testo.ORDINE;
				ordine = searchOrdineAfterLine(text, search);	
				break;
			}


			break;

		case Testo.ORTOFIN:
			search = "PAG 1";
			ordine = searchOrdineBeforeLine(text, search);
			break;
		case Testo.LANDO:
			ordine.setNumero("0");
			ordine.setData(toFormat.format(new Date()));
			break;

		default:
			break;
		}

		return ordine;

	}

	private OrdineCliente searchOrdine(String s, String search, String regex){
		if (search != null) {
			for (String line : s.split("\r")) {
				if (line.toUpperCase().trim().contains(search)){

					if (type == Testo.SMA_FILE){
						int indexOf = line.indexOf("ORDINE NUMERO");
						line = line .substring(indexOf+13, line.length()); 
					}
					if (type == Testo.CONAD_QUILIANO){
						int indexOf = line.indexOf("Ordine Nr. :");
						line = line .substring(indexOf+10, line.length()); 
					}
					if (type == Testo.DALMARE){
						line = line.replaceAll("Ordine n°", "");
					}
					if (type == Testo.EASY_FISH || type == Testo.ARTELIER || type == Testo.FVM_PESCA || type == Testo.WORLD_FISH || type == Testo.LORENA){
						if (line.contains("Ordine n.")) {
							int indexOf = line.indexOf("Ordine n.");
							line = line .substring(indexOf+9, line.length());	
						}
					}
					if (type == Testo.TOSANO){
						int indexOf = line.indexOf("Ordine a Fornitore");
						line = line .substring(indexOf+18, line.length()); 
					}
					
					if (type == Testo.EUROSPIN){
						int indexOf = line.indexOf("N.Avv.Carico");
						line = line .substring(indexOf+13, line.length()); 
					}
					
					if (type == Testo.MEGAMARK){
						int indexOf = line.indexOf("CONFERMA ORDINE ACQUISTO");
						line = line .substring(indexOf+24, line.length()); 
					}

					Matcher m = Pattern.compile(regex).matcher(line);
					if (m.find()) {
//												System.out.println(m.group(1));
//												System.out.println(m.group(2));
//												System.out.println(m.group(3));
//												System.out.println(m.group(4));
//												System.out.println(m.group(5));

						String dataReplace ="";
						OrdineCliente ordine = new OrdineCliente();
						switch (type) {
						case Testo.SELECTA:
							ordine.setNumero(m.group(1));
							ordine.setData(toFormat.format(new Date()));
							break;
						case Testo.DESPAR:
							ordine.setNumero(m.group(1));
							try {
								ordine.setData(toFormat.format(fromFormat.parse(m.group(3))));	
							} catch (Exception e) {
								ordine.setData(toFormat.format(new Date()));
							}
							break;
						case Testo.ADC_SRL:
							ordine.setNumero(m.group(1));
							ordine.setData(m.group(2));
							break;
						case Testo.FIORITAL:
						case Testo.DALMARE:
							ordine.setNumero(m.group(2));
							ordine.setData(toFormat.format(new Date()));
							break;
						case Testo.ONE_SERVICE:
							ordine.setNumero(m.group(1));
							ordine.setData(toFormat.format(new Date()));
							break;
						case Testo.CONAD_QUILIANO:
							ordine.setNumero(m.group(2).replace("del", "").replace("-", ""));
							ordine.setData(m.group(3));
							break;
						case Testo.ESSELUNGA:
							ordine.setNumero(m.group(1));
							dataReplace = leftPadding(m.group(2).replace("DEL", ""));
							ordine.setData(dataReplace);
							break;
						case Testo.SMA_FILE:
							ordine.setNumero(m.group(1));
							dataReplace = leftPadding(m.group(3).replace("DEL", ""));
							ordine.setData(dataReplace);
							break;
						case Testo.COOP:
							ordine.setNumero(m.group(1));
							dataReplace = leftPadding(m.group(3).replace("DEL", ""));
							ordine.setData(dataReplace);
							break;
						case Testo.AUCHAN:
							ordine.setNumero(m.group(1));
							dataReplace = leftPadding(m.group(2).replace("DEL", ""));
							ordine.setData(dataReplace);
							break;
						case Testo.INTELLIGENTE:
							ordine.setNumero(m.group(2));
							dataReplace = leftPadding(m.group(4).replace("DEL", ""));
							ordine.setData(dataReplace);
							break;
//						case Testo.DALMARE:
//							ordine.setNumero(m.group(2));
//							dataReplace = leftPadding(m.group(4).replace("DEL", ""));
//							ordine.setData(dataReplace);
//							break;
						case Testo.WOERNDLE:
							ordine.setNumero(m.group(2));
							dataReplace = leftPadding(m.group(4).replace("OF", ""));
							ordine.setData(dataReplace);
							break;
						case Testo.TOSANO:
							ordine.setNumero(m.group(1));
							dataReplace = m.group(3);
							ordine.setData(dataReplace);
							break;
						case Testo.CONAD:
							ordine.setNumero(m.group(1));
							dataReplace = m.group(3);
							ordine.setData(dataReplace);
							break;
						case Testo.EASY_FISH:
						case Testo.ARTELIER:
						case Testo.FVM_PESCA:
						case Testo.WORLD_FISH:
						case Testo.LORENA:
							ordine.setNumero(m.group(2));
							dataReplace = leftPadding(m.group(4).replace("DEL", ""));
							ordine.setData(dataReplace);
							break;
						case Testo.EUROSPIN:
							ordine.setNumero(m.group(1));
							dataReplace = leftPadding(m.group(3).replace("DEL", ""));
							ordine.setData(dataReplace);
							break;
						case Testo.MEGAMARK:
							ordine.setNumero(m.group(1).replace("del", "").replace("V/", "").replace("R/", ""));
							try {
								ordine.setData(toFormat.format(fromFormat.parse(m.group(2))));	
							} catch (Exception e) {
								ordine.setData(toFormat.format(new Date()));
							}
							
							break;
						default:
							break;
						}

						return ordine;

					}
				}
			}
		}
		return null;
	}

	//	private Ordine searchOrdineConad(String s, String regex){
	//		for (String line : s.split("\r")) {
	//			if (line.toUpperCase().trim().contains(search)){
	//				
	//			if (type == Testo.SMA_FILE){
	//				int indexOf = line.indexOf("ORDINE NUMERO");
	//				line = line .substring(indexOf+13, line.length()); 
	//			}
	//			if (type == Testo.DALMARE){
	//				line = line.replaceAll("Ordine n�", "");
	//			}
	//			if (type == Testo.EASY_FISH || type == Testo.ARTELIER || type == Testo.FVM_PESCA || type == Testo.WORLD_FISH || type == Testo.LORENA){
	//				if (line.contains("Ordine n.")) {
	//					int indexOf = line.indexOf("Ordine n.");
	//					line = line .substring(indexOf+9, line.length());	
	//				}
	//			}
	//				
	//				Matcher m = Pattern.compile(regex).matcher(line);
	//				if (m.find()) {
	//	}

	private OrdineCliente searchOrdineBeforeLine(String s, String search){
		List<String> lines = new ArrayList<String>();

		if (search != null) {
			for (String line : s.split("\r")) {
				lines.add(line);
				if (line.toUpperCase().trim().contains(search)){
					break;
				}
			}
			OrdineCliente ordine = new OrdineCliente();
			String dataReplace  ="";

			switch (type) {
			case Testo.IPER:
				dataReplace = leftPadding(lines.get(lines.size()-3));
				ordine.setData(dataReplace);
				ordine.setNumero(lines.get(lines.size()-4));
				return ordine;
			case Testo.CONAD:
				String newLine = lines.get(lines.size()-2).trim();
				String [] value = newLine.split(" ");
				dataReplace = leftPadding(value[1]);
				ordine.setData(dataReplace);
				ordine.setNumero(value[0]);
				return ordine;
			case Testo.ORTOFIN:
				dataReplace = leftPadding(lines.get(lines.size()-3));
				ordine.setData(dataReplace);
				ordine.setNumero(lines.get(lines.size()-4));
				return ordine;
			default:
				break;
			}
		}
		return null;

	}

	private OrdineCliente searchOrdineInLine(String s, String search){
		List<String> lines = new ArrayList<String>();
		OrdineCliente ordine = new OrdineCliente();

		if (search != null) {
			for (String line : s.split("\r")) {
				lines.add(line);
				if (line.toUpperCase().trim().contains(search)){
					try {
						switch (type) {
						case Testo.TIGROS:
							int indexOf = line.indexOf("Prev.cons.");
							line = line.substring(0, indexOf); 
							String [] rowArray = line.trim().split("\\s+");
							int lenght = rowArray.length;
//							ordine.setData(leftPadding(rowArray[lenght-1]));
							ordine.setData(toFormat.format(fromFormat.parse(rowArray[rowArray.length-1])));
							ordine.setNumero(rowArray[lenght-3]);
							return ordine;
						case Testo.CONAD_VUOTO:
							int indexOf1 = line.indexOf("Ordine Nr.:");
							line = line.substring(indexOf1+11, line.length()); 
							String [] rowArray1 = line.trim().split("\\s+");
							int lenght1 = rowArray1.length;
							ordine.setData(leftPadding(rowArray1[lenght1-1]));
							ordine.setNumero(rowArray1[0]);
							return ordine;
						default:
							break;
						}	
					} catch (Exception e) {
						 
					}
					

				}

			}

		}
		return ordine;
	}
	
	private OrdineCliente searchOrdineCrisFish(String s){
		List<String> lines = new ArrayList<String>();
		OrdineCliente ordine = new OrdineCliente();

		 	for (String line : s.split("\r")) {
				lines.add(line);
				if (line.toUpperCase().trim().contains("N.")){
					ordine.setNumero(line.replaceAll("n.", "").trim());
				}	
				
				String regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				Matcher m = Pattern.compile(regex).matcher(line);
				if (m.find() && ordine.getData() == null) {
					
					try {
						Date parse = toFormat.parse(m.group(1));
						ordine.setData(toFormat.format(parse));
						break;
					} catch (Exception e) {

					}
				 

			}

		}
		return ordine;
	}


	private OrdineCliente searchOrdineAfterLine(String s, String search){
		List<String> lines = new ArrayList<String>();
		int i=0;
		boolean add = false;
		boolean exit = false;
		if (search != null) {
			for (String line : s.split("\r")) {
				if (line.toUpperCase().trim().contains(search)){
					add = true;
				}
				if (add){
					lines.add(line);
					i++;

					switch (type){
					case Testo.GS:
						if (i > 3){
							exit = true;
						}
						break;
					case Testo.CARREFOUR:
						if (i > 2){
							exit = true;
						}
						break;
					}

					if (exit) {
						break;
					}

				}

			}

			String dataReplace  ="";
			OrdineCliente ordine = new OrdineCliente();

			switch (type) {
			case Testo.CARREFOUR:
			case Testo.GS:
				dataReplace = leftPadding(lines.get(lines.size()-1));
				ordine.setData(dataReplace);
				ordine.setNumero(lines.get(lines.size()-2));
				break;
			case Testo.CENTRALE:
				String [] value = lines.get(1).split("/");
				dataReplace = leftPadding(value[1]);
				ordine.setData(dataReplace);
				ordine.setNumero(value[0]);
				break;
			case Testo.COOP:
				String [] value1 = lines.get(1).split("/");
				dataReplace = leftPadding(value1[1]);
				ordine.setData(dataReplace);
				ordine.setNumero(value1[0]);
				break;
			case Testo.UNICOOP:
				String numero = lines.get(1).replace("N.", "").replaceAll("/0", "").trim();
				ordine.setNumero(numero);
				dataReplace = leftPadding(lines.get(2).replace("Del", "").trim());
				ordine.setData(dataReplace);
				break;
			default:
				break;
			}


			return ordine;

		}
		return null;

	}

	private OrdineCliente searchOrdineCustom(String s, String search){
		List<String> lines = new ArrayList<String>();
		OrdineCliente ordine = new OrdineCliente();

		if (search != null) {
			for (String line : s.split("\r")) {
				lines.add(line);
				if (line.toUpperCase().trim().contains(search)){
					ordine.setNumero(line.trim());
				}

				String regex = "(\\d{2}/\\d{2}/\\d{4})";
				Matcher m = Pattern.compile(regex).matcher(line);
				if (m.find() && ordine.getData() == null) {
					ordine.setData(m.group(1));
				}

			}

		}
		return ordine;

	}
	
	private OrdineCliente searchOrdineAdc(String s, String search1, String search2){

		OrdineCliente ordine = new OrdineCliente();
		boolean isNumero = false;
		boolean isData = false;
		
			for (String line : s.split("\r")) {
				 
				if (line.toUpperCase().trim().contains(search1)){
					isNumero = true;
					continue;
				}
				
				if (isNumero) {
					ordine.setNumero(line.trim());
					isNumero=false;
				}
				
				if (line.toUpperCase().trim().contains(search2)){
					isData = true;
					continue;
				}
				
				if (isData) {
					ordine.setData(line.trim());
					break;
				}
			}
		 
		return ordine;

	}

// OLD VERSION	
	private OrdineCliente searchOrdineNederlof(String s, String search1, String search2){
		List<String> lines = new ArrayList<String>();
		OrdineCliente ordine = new OrdineCliente();

		for (String line : s.split("\r")) {
			lines.add(line);

			if (line.toUpperCase().trim().contains(search1)){
				line = line.replace(search1, "");
				ordine.setNumero(line.trim());
			}

			if (line.toUpperCase().trim().contains(search2)){
				line = line.replace(search2, "").trim();

				String regex = "(\\d{1,2}/\\d{2}/\\d{4})";
				Matcher m = Pattern.compile(regex).matcher(line);
				if (m.find() && ordine.getData() == null) {
					
					try {
						Date parse = toFormat.parse(m.group(1));
						ordine.setData(toFormat.format(parse));
					} catch (Exception e) {

					}
					
				}
			}

		}


		return ordine;

	}
	
	// NUOVA VERSIONE
//	private OrdineCliente searchOrdineNederlof(String s, String search1, String search2){
//		
//		OrdineCliente ordine = new OrdineCliente();
//
//		for (String line : s.split("\r")) {
//		
//				regex = "([0-9]+)(.*)(\\d{2}/\\d{2}/\\d{2})(.*)";
//				Matcher m = Pattern.compile(regex).matcher(line);
//				if (m.find() && ordine.getData() == null) {
//					
//					try {
//						
//						String data = toFormat.format(fromFormat.parse(m.group(3)));
//						ordine.setData(data);
//						String[] rowArray = m.group(2).trim().split("\\s+");
//						ordine.setNumero(rowArray[rowArray.length-2]);
//					} catch (Exception e) {
//
//					}
//					
//				}
//	
//		}
//		
//		return ordine;
//	}

	private String leftPadding(String data){
		String newData = StringUtils.leftPad(data, 10, "0");
		newData = newData.replace("-", "/");
		newData = newData.replace(".", "/");
		String output;
		try {
			output = toFormat.format(fromFormat.parse(newData));
		} catch (ParseException e) {
			output = newData;
		}
		return output;
	}


}
