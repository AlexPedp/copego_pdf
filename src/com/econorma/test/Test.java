package com.econorma.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Test {
	
	 public static void main(String[] args) throws Exception {
	        // Get the input stream of excel file
	        InputStream inputStream = new FileInputStream(
	                "AXB RO 02 09 2017 SABATO.xlsm");
	        // Create a workbook object.
	        Workbook wb = WorkbookFactory.create(inputStream);
	        Sheet sheet = wb.getSheetAt(1);
	       
	        // Iterate over all the row and cells
	        for (Iterator<Row> rit = sheet.rowIterator(); rit.hasNext();) {
	            Row row = rit.next();
	            boolean found = false;
	            for (Iterator<Cell> cit = row.cellIterator(); cit.hasNext();) {
	                Cell cell = cit.next();
	            	 int column = cell.getColumnIndex();
	            	 
	            	 if (found && column == 16){
		            	String a = ""; 
	            	 }
	            	 
	            	 String value = getCellValueAsString(cell);
	            	 
	            	 
	            	 if (column == 1 && value.contains("MITILO")){
	            		found = true;
	            	 }
	            	 
	            	 if (found){
	            		 System.out.print(value);
		  	             System.out.print(";"); 
	            	 }
	            	
	  	                
	            	
	            
	              
	            }
	            System.out.println("");
	        }
	        
	        
	    }
	 
	 
	    /**
	     * This method for the type of data in the cell, extracts the data and
	     * returns it as a string.
	     */
	    public static String getCellValueAsString(Cell cell) {
	        String strCellValue = null;
	        if (cell != null) {
	        	
	        	DataFormatter formatter = new DataFormatter();  
	         	
	            switch (cell.getCellType()) {
	            case Cell.CELL_TYPE_STRING:
	                strCellValue = cell.toString();
	                break;
	            case Cell.CELL_TYPE_NUMERIC:
	                if (DateUtil.isCellDateFormatted(cell)) {
	                    SimpleDateFormat dateFormat = new SimpleDateFormat(
	                            "dd/MM/yyyy");
	                    strCellValue = dateFormat.format(cell.getDateCellValue());
	                } else {
	                    Double value = cell.getNumericCellValue();
	                    Long longValue = value.longValue();
	                    strCellValue = new String(longValue.toString());
	                }
	                break;
	            case Cell.CELL_TYPE_BOOLEAN:
	                strCellValue = new String(new Boolean(
	                        cell.getBooleanCellValue()).toString());
	                break;
	            case Cell.CELL_TYPE_FORMULA:
	            	strCellValue = null;
	             	
	            	 switch(cell.getCachedFormulaResultType()) {
	                 case Cell.CELL_TYPE_NUMERIC:
	                	 strCellValue = String.valueOf(cell.getNumericCellValue());
	                     break;
	                 case Cell.CELL_TYPE_STRING:
	                	 strCellValue = String.valueOf(cell.getRichStringCellValue());
	                     break;
	             }
 
	                break;
	            case Cell.CELL_TYPE_BLANK:
	                strCellValue = "";
	                break;
	            }
	        }
	        return strCellValue;
	    }
 

}
