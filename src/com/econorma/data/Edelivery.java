package com.econorma.data;

public class Edelivery {
	
	private String addr1;
	private String addr2;
	private String city;
	private String email;
	private String fullname;
	private String country;
	private String phone;
	private String state;
	private String userUID;
	private String vat;
	private String zip;
	private String stateName;
	private String stateISO;
	
	public String getAddr1() {
		return addr1;
	}
	
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	
	public String getAddr2() {
		return addr2;
	}
	
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFullname() {
		return fullname;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getUserUID() {
		return userUID;
	}
	
	public void setUserUID(String userUID) {
		this.userUID = userUID;
	}
	
	public String getVat() {
		return vat;
	}
	
	public void setVat(String vat) {
		this.vat = vat;
	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateISO() {
		return stateISO;
	}

	public void setStateISO(String stateISO) {
		this.stateISO = stateISO;
	}
	
}
