package com.econorma.data;

public class Eorder {

	private String orderID;
	private Double total;
	private String orderDate;
	private int status;
	private Edetail details;
	
	private EdetailApp detailsApp;

	public String getOrderID() {
		return orderID;
	}

	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public Edetail getDetails() {
		return details;
	}

	public void setDetails(Edetail details) {
		this.details = details;
	}
	
	public EdetailApp getDetailsApp() {
		return detailsApp;
	}

	public void setDetailsApp(EdetailApp detailsApp) {
		this.detailsApp = detailsApp;
	}

	@Override
	public String toString(){
		return getOrderID() + "| " + getOrderDate() + "|" + getStatus();
	}

}
