package com.econorma.data;

import java.util.Map;

import com.google.gson.annotations.SerializedName;

public class Ecart {
	
	private Double total;
	@SerializedName("items")
	private Map<String, Eitem> items;
	private String  saleCode;
	private Double saleAmount;
	private String  couponCode;
	
	public Double getTotal() {
		return total;
	}
	
	public void setTotal(Double total) {
		this.total = total;
	}

	public Map<String, Eitem> getItems() {
		return items;
	}

	public void setItems(Map<String, Eitem> items) {
		this.items = items;
	}

	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	public Double getSaleAmount() {
		return saleAmount;
	}

	public void setSaleAmount(Double saleAmount) {
		this.saleAmount = saleAmount;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	
}
