package com.econorma.data;

public class Eitem {
	
	private int count;
	private Double price;
	private String title;
	private Double listing_discount;
	private Double listing_price;
	private Double tax_profile_value;
	
	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public Double getPrice() {
		return price;
	}
	
	public void setPrice(Double price) {
		this.price = price;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Double getListing_discount() {
		return listing_discount;
	}
	
	public void setListing_discount(Double listing_discount) {
		this.listing_discount = listing_discount;
	}
	
	public Double getListing_price() {
		return listing_price;
	}
	
	public void setListing_price(Double listing_price) {
		this.listing_price = listing_price;
	}
	
	public Double getTax_profile_value() {
		return tax_profile_value;
	}
	
	public void setTax_profile_value(Double tax_profile_value) {
		this.tax_profile_value = tax_profile_value;
	}
	
	@Override
	public String toString(){
		return getTitle() + "|" + getCount() + "|" + getListing_price() + "|" + getTax_profile_value(); 
	}
	
}
