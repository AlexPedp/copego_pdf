package com.econorma.data;

public class Edetail {
	
	private String orderID;
	private Ebilling billing;
	private Edelivery delivery;
	private Ecart cart;
	private Double total;
	private String userUID;
	private String method;
	private String payment;
	private String status;
	private String transactionID;
	
	public String getOrderID() {
		return orderID;
	}
	
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	
	
	public Ebilling getBilling() {
		return billing;
	}

	public void setBilling(Ebilling billing) {
		this.billing = billing;
	}

	public Edelivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Edelivery delivery) {
		this.delivery = delivery;
	}

	public Double getTotal() {
		return total;
	}
	
	public void setTotal(Double total) {
		this.total = total;
	}
	
	public String getUserUID() {
		return userUID;
	}
	
	public void setUserUID(String userUID) {
		this.userUID = userUID;
	}
	
	public String getMethod() {
		return method;
	}
	
	public void setMethod(String method) {
		this.method = method;
	}
	
	public String getPayment() {
		return payment;
	}
	
	public void setPayment(String payment) {
		this.payment = payment;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getTransactionID() {
		return transactionID;
	}
	
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public Ecart getCart() {
		return cart;
	}

	public void setCart(Ecart cart) {
		this.cart = cart;
	}

	@Override
	public String toString(){
		return getOrderID() + "| "+ getPayment() + "|" + getUserUID() + '|' + getBilling().getFullname() + "|" + getBilling().getCountry() + "|" + getDelivery().getAddr1();
	}
}
