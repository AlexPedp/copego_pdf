package com.econorma.data;

public class Ecustomer {

	private String fullname;
	private String email;
	private String created_at;
	private String id;

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString(){
		return getFullname() + ", "+ getEmail() + ", "+ getId() + "," + getCreated_at();
	}

}
