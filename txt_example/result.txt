﻿ ESSELUNGA S.P.A.                             M/2001165525-PAG.  1                      00000
                                                                                              
IMPORTANTE : COMUNICARE OGNI VARIAZIONE RELATIVA A QUANTITATIVO 0                             
GIORNI DI CONSEGNA                                                                            
SPETTABILE                                                                                    
CONSORZIO PESCATORI DI GORO SO                                 152862                         
VIA ANTONIO BRUGNOLI 298                                                                      
44020 GORO                         FE                                                         
ALLE NOSTRE CONDIZIONI GENERALI D' ACQUISTO A VOI GIA' COMUNICATE                             
CON ACCORDO QUADRO N“ 000775.00/2020                                                          
VI TRASMETTIAMO NS. ORDINE D'ACQUISTO N.RO M/2001165525 DEL 17/12/20                          
CONSEGNARE TASSATIVAMENTE IL 18/12/20                                                         
FRANCO RIBALTA NOSTRO MAGAZZINO                                                               
DI BIANDRATE (NO) STRADA PROVINCIALE BIANDRATE-RECETTO                                        
INDICARE IN FATTURA E SUI DOCUMENTI DI TRASPORTO IL NUMERO D’ ORDINE                          
DI ACQUISTO E IL NUMERO ACCORDO                                                               
                                                                                              
ARTICOLO          DESCRIZIONE                       COLLI KG.                COSTO            
799599            COZZE SOTTOVUOTO                    130 520,00                        2,25  
                  Mytilus galloprovincialis                                                   
799649            VONGOLE VERACI 500 GR.              710 1 .420,00                     9,80  
                  Ruditapes philippinarum                                                     
799856            VONGOLA VERACE                      150 750,00                        8,40  
                  Ruditapes philippinarum                                                     
799883            LUPINI 500 G                         40            60,00              5,36  
- Chamelea gallina                                                                            
*** TOTALE COLLI / KG.                             1 .030 2.750,00                            
SALVO DIVERSO ACCORDO TRA LE PARTI, I              PREZZI SONO COMPRENSIVI   DI               
SPESE DI CONFEZIONAMENTO, IMBALLAGGIO, TRASPORTO E SCARICO PRESSO LA                          
RIBALTA DEL MAGAZZINO DI DESTINAZIONE INDICATO.                                               
I CONTRATTI DI TRASPORTO RELATIVI ALLE MERCI DOVRANNO CONTENERE                               
ESPRESSAMENTE LA CLAUSULA PORTO FRANCO.                                                       
PAGAMENTO: BONIFICO            30 GG.                                                         
DATA RICEVIMENTO FATTURA (NON ANTERIORE AL RICEVIMENTO MERCE).                                
DISTINTI SALUTI                                                                               
                                                                                              
                                    ESSELUNGA S.P.A.                                          
